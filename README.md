# No to co jemy ?

### Co trzeba aby jeść ?
 - Docker
 - Compose
 - VirtualBox

### Jak jeść ?

Uwaga! Tylko dla orłów - budowa obrazów:

Buduje obraz php (konfiguracja w docker-compose.devops.yml)
```
$ docker-compose -f docker-compose.devops.yml build php
```
Wysyła obraz php do rejestru

```
$ docker-compose -f docker-compose.devops.yml push php
```

W normalnej sytuacji, po prostu pobieramy obrazy:
```
$ docker-compose pull
```

Uruchamiamy aplikację:

```
$ docker-compose up -d
```

Instalujemy zależności:

```
$ docker-compose exec cli composer install --no-interaction --optimize-autoloader
```

Odpalamy migracje:

```
$ docker-compose exec cli composer dev
```

Pliki konfiguracyjne aplikacji znajdują się w katalogu `/etc`.

Konfiguracja z `etc` mapowana jest do kontenerów `nginx` i `php`, **NIE JEST** mapowana do `cli`.

Domyślnie:

1. Kontener `php` korzysta z obrazu z zainstalowanym XDebug'iem - na potrzeby produkcji można to zmienić zmieniając nazwę obrazu z `registry.tshdev.io/cojemy/cojemy-php:dev` na `registry.tshdev.io/cojemy/cojemy-php:prod`
2. Kontener `cli` używa obrazu `registry.tshdev.io/cojemy/cojemy-php:prod` - xdebug nie jest zalecany w konsoli.

Domyślnie aplikacja powinna być dostępna pod adresem IP:

```
http://twoje-ip/
```

### Jak testować jedzenie ?

```
$ docker-compose exec cli bin/behat
```

```
$ docker-compose exec cli bin/phpspec run
```

lub jako polecenia composera zawierające 2 powyższe
```
docker-compose exec cli composer test
```