@domain
Feature: Update position from order
  In order to manage order positions
  As a user
  I want to be able update previously added position

  Scenario: Position updating
    Given there are no orders in the system
    And there is order in the system with id "order123" with positions:
      | dishName    | dishPrice | dishId | userId | userNick | positionId |
      | burger      | 1900     | dish1  | user1  | jack     | position1  |
    When user "user1" updates position with dishId "dish1" from order "order123" with values:
      | dishName        | dishPrice | dishId | userId | userNick |
      | new burger      | 2900      | dish2  | user1  | john     |
    Then the order "order123" should contain "1" positions
    And the position should be "new burger", "dish2", "2900" owned by "john"

  Scenario: Calculating total price after updating position
    Given there are no orders in the system
    And there is order in the system with id "order123" with positions:
      | dishName    | dishPrice | dishId | userId | userNick | positionId |
      | burger      | 1900     | dish1  | user1  | jack     | position1  |
      | pizza       | 2300     | dish2  | user2  | anne     | position2  |
    When user "user1" updates position with dishId "dish1" from order "order123" with values:
      | dishName        | dishPrice | dishId | userId | userNick |
      | new burger      | 2900      | dish2  | user1  | john     |
    Then there should be 1 order with 2 positions
    And order total price should be "8200"
