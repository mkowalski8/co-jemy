Feature: Add position to order

  Background:
    Given the database is empty
    And there are no orders in the system

  Scenario: Add position with userId header
    Given there is order in the system with id "5"
    When I send a "POST" request to "/api/orders/5/positions" with body and headers:
    """
    {
      "headers":
      {
        "userId":"user1"
      },
      "body":
      {
        "dishId":"dish1",
        "dishName":"burger",
        "price":1900,
        "userNick":"nick1"
      }
    }
    """
    Then the response code should be 201
    And the response should contain json:
    """
    {
      "position":
      {
        "userId":"user1",
        "id":"@string@",
        "dishId":"dish1",
        "dishName":"burger",
        "username":"nick1",
        "price":1900
      }
    }
    """

  Scenario: Add position without userId
    Given there is order in the system with id "5"
    When I send a "POST" request to "/api/orders/5/positions" with body:
    """
    {
      "dishId":"dish1",
      "dishName":"burger",
      "price":1900,
      "userNick":"nick1"
    }
    """
    Then the response code should be 201
    And there should be "1" order with "1" position
    And the position should be "burger", "dish1", "1900" owned by "nick1"
    And the response should contain json:
    """
    {
      "position":
        {
          "userId":"@string@",
          "id":"@string@",
          "dishId":"dish1",
          "dishName":"burger",
          "username":"nick1",
          "price":1900
        }
    }
    """

  Scenario: Add position when order is not found
    Given there is order in the system with id "2"
    When I send a "POST" request to "/api/orders/8/positions" with body:
    """
    {
      "dishId":"dish1",
      "dishName":"burger",
      "price":"190",
      "userNick":"nick1"
    }
    """
    Then the response code should be 404
    And the response should contain json:
    """
    {
      "code":404,
      "message":"There is no order with id 8."
    }
    """

  Scenario: Add position when order is closed
    Given there is order in the system with id "2"
    And I close order with id "2"
    When I send a "POST" request to "/api/orders/2/positions" with body:
    """
    {
      "dishId":"dish1",
      "dishName":"burger",
      "price":"190",
      "userNick":"nick1"
    }
    """
    Then the response code should be 401
    And the response should contain json:
    """
    {
      "code":401,
      "message":"Order with id 2 has been closed."
    }
    """
