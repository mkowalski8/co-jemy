@domain
Feature: Close order
  As a user I want to...
  So that I...

  Scenario:
    Given there are no orders in the system
    And there is order in the system with id "toClose"
    When I close order with id "toClose"
    Then there should be "1" order with status "closed"
