@domain
Feature: Remove position from order
  In order to manage order positions
  As a user
  I need to remove previously added position

  Scenario: Position removing
    Given there are no orders in the system
    And there is order in the system with id "order123" with positions:
     | dishName    | dishPrice | dishId | userId | userNick | positionId |
     | burger      | 1900     | dish1  | user1  | jack     | position1  |
     | pizza       | 2300     | dish2  | user2  | anne     | position2  |
    When User "user1" removes dish "dish1" from order "order123"
    Then the order "order123" should contain "1" positions
    And the position should be "pizza", "dish2", "2300" owned by "anne"

  Scenario: Calculating total price after removing position
    Given there are no orders in the system
    And there is order in the system with id "order123" with positions:
      | dishName    | dishPrice | dishId | userId | userNick | positionId |
      | burger      | 1900     | dish1  | user1  | jack     | position1  |
      | pizza       | 2300     | dish2  | user2  | anne     | position2  |
    When User "user1" removes dish "dish1" from order "order123"
    Then there should be 1 order with 1 position
    And order total price should be "4300"

  @payment
  Scenario: Position removing with attached payment
    Given there are no orders in the system
    And there is order in the system with id "order123" with positions:
      | dishName    | dishPrice | dishId | userId | userNick | positionId |
      | burger      | 1900     | dish1  | user1  | jack     | position1  |
    When I add payment for order position with payer "Jon" and value "1000" for order with id "order123"
    And User "user1" removes dish "dish1" from order "order123"
    Then the order "order123" should contain "1" payment
    And the payment should be "Jon", "1000" without position id attached
