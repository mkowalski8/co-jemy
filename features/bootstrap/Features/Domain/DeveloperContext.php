<?php

namespace Features\Domain;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\TableNode;
use Behat\Symfony2Extension\Context\KernelDictionary;

use Domain\SharedKernel\Money;
use Infrastructure\CoJemy\Order\Commands\UpdatePositionCommand;
use League\Tactician\CommandBus;

use Infrastructure\CoJemy\Order\Commands\AddPaymentToOrderCommand;
use Infrastructure\CoJemy\Order\Commands\AddPositionToOrderCommand;
use Infrastructure\CoJemy\Order\Commands\RemovePositionFromOrderCommand;
use Infrastructure\CoJemy\Order\Commands\OpenOrderCommand;
use Infrastructure\CoJemy\Order\Commands\CloseOrderCommand;

use Domain\CoJemy\Order;
use Domain\CoJemy\Aggregate\AggregateId;

use Tests\Builder\OrderBuilder;
use Tests\Repository\DatabaseEventStore;
use Tests\Repository\DatabaseSupplierRepository;

/**
 * Defines application features from the specific context.
 */
class DeveloperContext implements Context, SnippetAcceptingContext
{
    const DEFAULT_CURRENCY = 'PLN';
    use KernelDictionary;

    /**
     * @Given there are no orders in the system
     */
    public function thereAreNoOrdersInTheSystem()
    {
        $this->getEventStore()->clear();
    }

    /**
     * @Given there is only a supplier with id :supplierId and delivery cost :deliveryCost and price per package :pricePerPackage
     */
    public function thereIsOnlySupplierWithSpecifiedParameters($supplierId, $deliveryCost, $pricePerPackage)
    {
        $this->getSupplierRepository()->clear();
        $this->getSupplierRepository()->addSupplier($supplierId, $deliveryCost, $pricePerPackage);
    }

    /**
     * @When I open new order for supplier with id :supplierId and aggregate id :aggregateId and admin id :adminId
     */
    public function iOpenNewOrder($supplierId, $aggregateId, $adminId)
    {
        $this->getCommandBus()->handle(new OpenOrderCommand($supplierId, $aggregateId, $adminId));
    }

    /**
     * @Then there should be :ordersNumber order with status :status
     */
    public function thereShouldBeOrderWithStatus($ordersNumber, $status)
    {
        if ($this->getEventStore()->count() != $ordersNumber) {
            throw new \RuntimeException(sprintf('Bad count of orders, expected %s got %s', $ordersNumber, $this->getEventStore()->count()));
        }

        if ($this->getEventStore()->getOrder()->getStatus() != $status) {
            throw new \RuntimeException(sprintf('Bad status, expected %s got %s', $status, $this->getEventStore()->getOrder()->getStatus()));
        }
    }

    /**
     * @Then order should be placed for supplier :supplierName
     */
    public function orderShouldBePlacedForSupplier($supplierName)
    {
        if ($this->getEventStore()->getOrder()->getSupplierId() != $supplierName) {
            throw new \RuntimeException(sprintf('Bad supplier, expected %s got %s', $supplierName, $this->getEventStore()->getOrder()->getSupplierId()));
        }
    }

    /**
     * @Given there is order in the system with id :aggregateId
     */
    public function thereIsOrderInTheSystemWithId($aggregateId)
    {
        $order = (new OrderBuilder())
            ->withAggregateId($aggregateId)
            ->build();

        $this->getEventStore()->persist($order->getLatestEvents());
    }

    /**
     * @Given there is order in the system with id :aggregateId and :pricePerPackage price per package and :deliveryCost delivery cost
     */
    public function thereIsOrderInTheSystemWithIdAndPricePerPackage($aggregateId, int $pricePerPackage, int $deliveryCost)
    {
        $order = (new OrderBuilder())
            ->withAggregateId($aggregateId)
            ->withPricePackage($pricePerPackage)
            ->withDeliveryCost($deliveryCost)
            ->build();

        $this->getEventStore()->persist($order->getLatestEvents());
    }

    /**
     * @When I close order with id :aggregateId
     */
    public function iCloseOrderWithId($aggregateId)
    {
        $this->getCommandBus()->handle(new CloseOrderCommand(AggregateId::fromString($aggregateId)));
    }

    /**
     * @Then order with id :aggregateId should have status :status
     */
    public function orderWithIdShouldHaveStatus($aggregateId, $status)
    {
        $order = $this->getEventStore()->findOrderById(AggregateId::fromString($aggregateId));

        if (!$order instanceof Order) {
            throw new \RuntimeException('Order with id ' . $aggregateId . ' not found');
        }

        if ($status !== (string) $order->getStatus()) {
            throw new \RuntimeException(sprintf(
                'Order with id %s has status %s instead of %s',
                $aggregateId,
                (string) $order->getStatus(),
                $status
            ));
        }
    }

    /**
     * @Then order should have delivery cost set to :deliveryCost and price per package to :pricePerPackage
     */
    public function orderShouldHaveDeliveryCostSetToAndPricePerPackageTo($deliveryCost, $pricePerPackage)
    {
        $actualDeliveryCost = $this->getEventStore()->getOrder()->getPrices()->getDeliveryCost()->getAmount();
        $expectedDeliveryCost = new Money($deliveryCost, new Money\Currency(self::DEFAULT_CURRENCY));

        if (!$actualDeliveryCost->isEqualTo($expectedDeliveryCost)) {
            throw new \RuntimeException(
                sprintf('Expected %s but get %s delivery cost', $expectedDeliveryCost, $actualDeliveryCost)
            );
        }

        $actualPricePerPackage = $this->getEventStore()->getOrder()->getPrices()->getPricePerPackage()->getAmount();
        $expectedPricePerPackage = new Money($pricePerPackage, new Money\Currency(self::DEFAULT_CURRENCY));

        if (!$actualPricePerPackage->isEqualTo($expectedPricePerPackage)) {
            throw new \RuntimeException(
                sprintf('Expected %s but get %s price per package', $actualPricePerPackage, $expectedPricePerPackage)
            );
        }
    }

    /**
     * @When user :userId adds position with :dishName, :dishId and :dishPrice to order :orderId for :userNick
     */
    public function userAddsPositionWithAndToOrderFor($userId, $dishName, $dishId, $dishPrice, $orderId, $userNick)
    {
        $this->getCommandBus()->handle(new AddPositionToOrderCommand($orderId, $dishId, $dishName, $dishPrice, self::DEFAULT_CURRENCY, $userNick, $userId));
    }

    /**
     * @When I add payment for position with id :positionId with payer :payerName and value :paymentAmount for order with id :orderId
     */
    public function iAddPaymentForPositionWithId($positionId, $payerName, $paymentAmount, $orderId)
    {
        $this->getCommandBus()->handle(new AddPaymentToOrderCommand($orderId, $positionId, $payerName, $paymentAmount, self::DEFAULT_CURRENCY));
    }

    /**
     * @When I add payment for order position with payer :payerName and value :paymentAmount for order with id :orderId
     */
    public function iAddPaymentForPosition($payerName, $paymentAmount, $orderId)
    {
        $order = $this->getEventStore()->getOrder();
        $positionId = $order->getPositions()->toArray()[0]->getId();

        $this->getCommandBus()->handle(new AddPaymentToOrderCommand($orderId, (string) $positionId, $payerName, $paymentAmount, self::DEFAULT_CURRENCY));
    }

    /**
     * @Then there should be :ordersNumber order with :count position(s)
     */
    public function thereShouldBeOrderWithPosition($ordersNumber, $count)
    {
        if ($this->getEventStore()->count() != $ordersNumber) {
            throw new \RuntimeException(sprintf('Bad count of orders, expected %s got %s', $ordersNumber, $this->getEventStore()->count()));
        }

        if ($this->getEventStore()->getOrder()->getPositions()->count() != $count) {
            throw new \RuntimeException(sprintf('Bad count of positions, expected %s got %s', $count, $this->getEventStore()->getOrder()->getPositions()->count()));
        }
    }

    /**
     * @Then the position should be :dishName, :dishId, :dishPrice owned by :userNick
     */
    public function thePositionShouldBeOwnedBy($dishName, $dishId, $dishPrice, $userNick)
    {
        /** @var Order $order */
        $order = $this->getEventStore()->getOrder();
        $position = $order->getPositions()->toArray()[0];

        if ($position->getDish()->getName() != $dishName) {
            throw new \RuntimeException(sprintf('Bad dish name, expected %s got %s', $dishName, $position->getDish()->getName()));
        }

        if ($position->getDish()->getId() != $dishId) {
            throw new \RuntimeException(sprintf('Bad dish id, expected %s got %s', $dishId, $position->getDish()->getId()));
        }

        $expectedDishPrice = new Money($dishPrice, new Money\Currency(self::DEFAULT_CURRENCY));
        $dishPrice = $position->getDish()->getPrice()->getAmount();

        if (!$dishPrice->isEqualTo($expectedDishPrice)) {
            throw new \RuntimeException(sprintf('%s is different than expected %s dish price', $dishPrice, $expectedDishPrice));
        }

        if ($position->getUserNick() != $userNick) {
            throw new \RuntimeException(sprintf('Bad user nick for dish, expected %s got %s', $userNick, $position->getUserNick()));
        }
    }

    /**
     * @Then the payment should be :payerName, :paymentAmount, :positionId
     */
    public function thePaymentsShouldBe($payerName, $paymentAmount, $positionId)
    {
        /** @var Order $order */
        $order = $this->getEventStore()->getOrder();
        $payment = $order->getPayments()->toArray()[0];

        if ($payment->getPayerName() != $payerName) {
            throw new \RuntimeException(sprintf('Bad payer name, expected %s got %s', $payerName, $payment->getPayerName()));
        }

        $expectedPayment = Money::fromString($paymentAmount, self::DEFAULT_CURRENCY);
        if (!$payment->getPaymentValue()->isEqualTo($expectedPayment)) {
            throw new \RuntimeException(sprintf('Bad payment value, expected %s got %s', $expectedPayment->getAmount(), $payment->getPaymentValue()->getAmount()));
        }

        if ((string) $payment->getPositionId() != $positionId) {
            throw new \RuntimeException(sprintf('Bad position id, expected %s got %s', $positionId, $payment->getPositionId()));
        }
    }

    /**
     * @Then the payment should be :payerName, :paymentAmount without position id attached
     */
    public function thePaymentsShouldBeWithoutPositionId($payerName, $paymentAmount)
    {
        /** @var Order $order */
        $order = $this->getEventStore()->getOrder();
        $payment = $order->getPayments()->toArray()[0];

        if ($payment->getPayerName() != $payerName) {
            throw new \RuntimeException(sprintf('Bad payer name, expected %s got %s', $payerName, $payment->getPayerName()));
        }

        $expectedPayment = Money::fromString($paymentAmount, self::DEFAULT_CURRENCY);
        if (!$payment->getPaymentValue()->isEqualTo($expectedPayment)) {
            throw new \RuntimeException(sprintf('Bad payment value, expected %s got %s', $expectedPayment->getAmount(), $payment->getPaymentValue()->getAmount()));
        }

        if ($payment->getPositionId()->getValue() != null) {
            throw new \RuntimeException(sprintf('Position should be null, %s given',$payment->getPositionId()->getValue()));
        }
    }

    /**
     * @When noid user adds position with :dishName, :dishId and :dishPrice to order :orderId for :userNick
     */
    public function iNoIdUserAddPositionWithAndToOrderFor($dishName, $dishId, $dishPrice, $orderId, $userNick)
    {
        $this->getCommandBus()->handle(new AddPositionToOrderCommand($orderId, $dishId, $dishName, $dishPrice, self::DEFAULT_CURRENCY, $userNick));
    }

    /**
     * @Then the userId should be string
     */
    public function theUserIdShouldBeString()
    {
        /** @var Order $order */
        $order = $this->getEventStore()->getOrder();
        $position = $order->getPositions()->toArray()[0];

        if (!$position->getUserId() instanceof Order\UserId) {
            throw new \RuntimeException('User id is not instance of the class UserId');
        }

        if (!is_string((string) $position->getUserId())) {
            throw new \RuntimeException('User id is not string');
        }

        if (strlen((string) $position->getUserId()) == 0) {
            throw new \RuntimeException('User id is an empty string');
        }
    }

    /**
     * @Given order should have id :orderId
     */
    public function orderShouldHaveId($orderId)
    {
        if ($orderId !== (string)$this->getEventStore()->getOrder()->getAggregateId()) {
            throw new \RuntimeException('Order id is not equal to ' . $orderId);
        }
    }

    /**
     * @Given order should have admin hash :adminHash
     */
    public function orderShouldHaveAdminHash($adminHash)
    {
        if ($adminHash !== $this->getEventStore()->getOrder()->getHashHolder()->getAdminHash()) {
            throw new \RuntimeException('Admin hash is not equal to ' . $adminHash);
        }
    }

    /**
     * @Given order should have admin with id :adminId
     */
    public function orderShouldHaveAdminWithId($adminId)
    {
        foreach ($this->getEventStore()->getOrder()->getAdminIds() as $id) {
            if ($adminId === (string)$id) {
                return;
            }
        }

        throw new \RuntimeException('Admin id not found ' . $adminId);
    }

    /**
     * @Then order total price should be :amount
     */
    public function orderTotalPriceShouldBe($amount)
    {
        /** @var Order $order */
        $order = $this->getEventStore()->getOrder();
        $totalAmount = $order->getPrices()->getTotalAmount()->getAmount();

        $expectedTotalPrice = new Money($amount, new Money\Currency(self::DEFAULT_CURRENCY));

        if (!$totalAmount->isEqualTo($expectedTotalPrice)) {
            throw new \RuntimeException(sprintf('Expected %s total price amount, but was %s', $expectedTotalPrice, $totalAmount));
        }
    }
    /**
     * @Given there is order in the system with id :aggregateId with positions:
     */
    public function thereIsOrderWithPositions($aggregateId, TableNode $table)
    {
        $builder = new OrderBuilder();
        $builder->withAggregateId($aggregateId);
        foreach ($table as  $row) {
            $builder->withPosition($row);
        }
        $order = $builder->build();

        $this->getEventStore()->persist($order->getLatestEvents());
    }

    /**
     * @When User :userId removes dish :dishId from order :orderId
     */
    public function userRemovesDishFromOrder($userId, $dishId, $orderId)
    {
        $order = $this->getEventStore()->findOrderById(AggregateId::fromString($orderId));
        $positionToRemove = null;
        if ($positions = $order->getPositions()->toArray()) {
            foreach ($positions as $k => $position) {
                if ((string) $position->getUserId() == $userId
                    && $position->getDish()->getId() == $dishId) {
                    $positionToRemove = $position;
                    break;
                }
            }
        }

        if (null === $positionToRemove) {
            throw new \RuntimeException("Position not found");
        }

        $this->getCommandBus()->handle(new RemovePositionFromOrderCommand(AggregateId::fromString($orderId), $positionToRemove->getId(), Order\UserId::fromString($userId)));
    }

    /**
     * @When user :userId updates position with dishId :dishId from order :orderId with values:
     */
    public function userUpdatesPositionFromOrder($userId, $dishId, $orderId, TableNode $table)
    {
        $order = $this->getEventStore()->findOrderById(AggregateId::fromString($orderId));
        $positionToUpdate = null;
        if ($positions = $order->getPositions()->toArray()) {
            foreach ($positions as $position) {
                if ((string) $position->getUserId() == $userId
                    && $position->getDish()->getId() == $dishId) {
                    $positionToUpdate = $position;
                    break;
                }
            }
        }

        if (null === $positionToUpdate) {
            throw new \RuntimeException("Position not found");
        }

        $positionData = $table->getHash();

        $this->getCommandBus()->handle(
            new UpdatePositionCommand(
                (string) $positionToUpdate->getId(),
                $orderId,
                $positionData[0]['dishId'],
                $positionData[0]['dishName'],
                $positionData[0]['dishPrice'],
                self::DEFAULT_CURRENCY,
                $positionData[0]['userNick'],
                $userId
            )
        );
    }

    /**
     * @Then the order :orderId should contain :numPositions positions
     */
    public function theOrderShouldContainPositions($orderId, $numPositions)
    {
        $order = $this->getEventStore()->findOrderById(AggregateId::fromString($orderId));

        if (count($order->getPositions()) != $numPositions ) {
            throw new \RuntimeException(sprintf('Bad count of positions, expected %s given %s', $numPositions, count($order->getPositions())));
        }
    }

    /**
     * @Then the order :orderId should contain :numPayments payment(s)
     */
    public function theOrderShouldContainPayments($orderId, $numPayments)
    {
        $order = $this->getEventStore()->findOrderById(AggregateId::fromString($orderId));

        if (count($order->getPayments()) != $numPayments ) {
            throw new \RuntimeException(sprintf('Bad count of payments, expected %s given %s', $numPayments, count($order->getPayments())));
        }
    }

    /**
     * @return CommandBus
     */
    private function getCommandBus() : CommandBus
    {
        return $this->getContainer()->get('tactician.commandbus');
    }

    /**
     * @return DatabaseEventStore
     */
    private function getEventStore() : DatabaseEventStore
    {
        return $this->getContainer()->get('database_event_store');
    }

    /**
     * @return DatabaseSupplierRepository
     */
    private function getSupplierRepository() : DatabaseSupplierRepository
    {
        return $this->getContainer()->get('co_jemy.core.supplier_repository');
    }
}
