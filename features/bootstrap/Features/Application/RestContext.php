<?php

namespace Features\Application;

use Coduo\PHPMatcher\Factory\SimpleFactory;
use Behat\Mink\Driver\BrowserKitDriver;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Webmozart\Assert\Assert;

class RestContext extends RawMinkContext
{
    use KernelDictionary;

    /**
     * @When I send a :method request to :path
     */
    public function iSendAGetRequestTo($method, $path)
    {
        $driver = $this->getSession()->getDriver();

        if ($driver instanceof BrowserKitDriver) {
            $driver->getClient()->request($method, $path);
        } else {
            throw new \RuntimeException('Unsupported driver. BrowserKit driver is required.');
        }
    }

    /**
     * @When I send a :method request to :path with body:
     */
    public function iSendAGetRequestToWithBody($method, $path, PyStringNode $body)
    {
        $driver = $this->getSession()->getDriver();

        if ($driver instanceof BrowserKitDriver) {
            $driver->getClient()->request($method, $path, json_decode($body->getRaw(), true));
        } else {
            throw new \RuntimeException('Unsupported driver. BrowserKit driver is required.');
        }
    }

    /**
     * @When I send a :method request to :path with body and headers:
     */
    public function iSendAGetRequestToWithBodyAndHeaders($method, $path, PyStringNode $bodyAndHeaders)
    {
        $driver = $this->getSession()->getDriver();

        if ($driver instanceof BrowserKitDriver) {
            $bodyAndHeadersDecoded = json_decode($bodyAndHeaders->getRaw(), true);
            $body = $bodyAndHeadersDecoded['body'];
            $headers = $bodyAndHeadersDecoded['headers'];

            $headersProcessed = array_map(function ($key, $value) {
                return ['HTTP_' . strtoupper($key) => $value];
            }, array_keys($headers), $headers)[0];

            $driver->getClient()->request($method, $path, $body, [], $headersProcessed);
        } else {
            throw new \RuntimeException('Unsupported driver. BrowserKit driver is required.');
        }
    }

    /**
     * @Then the response code should be :code
     */
    public function theResponseCodeShouldBe($code)
    {
        $expected = intval($code);
        $actual = intval($this->getSession()->getStatusCode());
        Assert::same($expected, $actual);
    }

    /**
     * @Then the response should contain json:
     */
    public function theResponseShouldContainJson(PyStringNode $jsonString)
    {
        $factory = new SimpleFactory();
        $matcher = $factory->createMatcher();

        if (!$matcher->match($this->getSession()->getPage()->getContent(), $jsonString->getRaw())) {
            throw new \RuntimeException($matcher->getError());
        }
    }
}
