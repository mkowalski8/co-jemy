<?php

namespace spec\Bundle\CoJemyCore\CoreBundle\View;

use Application\View;
use Domain\CoJemy\Order\Dish;
use Domain\CoJemy\Order\Positions\Position;
use Domain\CoJemy\Order\Positions\UniqIdPositionId;
use Domain\CoJemy\Order\Prices\Price;
use Domain\CoJemy\Order\UserId;
use Domain\SharedKernel\Money;
use PhpSpec\ObjectBehavior;

class PositionViewSpec extends ObjectBehavior
{
    function it_returns_position_view_model(
        Position $position,
        Dish $dish,
        Price $price,
        Money $money
    ) {
        $id = UniqIdPositionId::fromString('positionId');
        $dishId = 'dishId';
        $dishName = 'dishName';
        $username = 'username';
        $priceAmount = 100;

        $userId = UserId::fromString('user123');

        $position->getId()->willReturn($id);
        $position->getUserNick()->willReturn($username);

        $dish->getId()->willReturn($dishId);
        $dish->getName()->willReturn($dishName);
        $price->getAmount()->willReturn($money);
        $money->getAmount()->willReturn($priceAmount);
        $dish->getPrice()->willReturn($price);
        $position->getDish()->willReturn($dish);

        $this->beConstructedWith($position, $userId);

        $this->shouldImplement(View::class);
        $this->toArray()->shouldReturn([
            'position' => [
                'userId' => (string) $userId,
                'id' => (string) $id,
                'dishId' => $dishId,
                'dishName' => $dishName,
                'username' => $username,
                'price' => $priceAmount
                ]
            ]
        );
    }
}
