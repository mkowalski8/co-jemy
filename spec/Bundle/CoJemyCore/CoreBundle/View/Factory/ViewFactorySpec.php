<?php

namespace spec\Bundle\CoJemyCore\CoreBundle\View\Factory;

use Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier;
use Bundle\CoJemyCore\CoreBundle\View\OrderView;
use Bundle\CoJemyCore\CoreBundle\View\PositionView;
use Doctrine\ORM\EntityRepository;
use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order;
use Domain\CoJemy\Order\Positions;
use Domain\CoJemy\Order\Positions\Position;
use Domain\CoJemy\Order\UserId;
use Infrastructure\CoJemy\Order\Exception\OrderNotFoundException;
use Infrastructure\CoJemy\Order\Exception\SupplierNotFoundException;
use PhpSpec\ObjectBehavior;

class ViewFactorySpec extends ObjectBehavior
{
    function let (EventStoreRepository $eventStoreRepository, EntityRepository $supplierRepository)
    {
        $this->beConstructedWith($eventStoreRepository, $supplierRepository);
    }

    function it_throws_an_error_when_could_not_find_order_in_repository(
        EventStoreRepository $eventStoreRepository
    ) {
        $aggregateId = AggregateId::fromString('id');
        $userId = UserId::fromString('user123');

        $eventStoreRepository->findOrderById($aggregateId)->willReturn(null);

        $this->shouldThrow(OrderNotFoundException::class)->during('createOrderView', [$aggregateId, $userId]);
    }

    function it_throws_an_error_when_could_not_find_supplier(
        EventStoreRepository $eventStoreRepository,
        EntityRepository $supplierRepository,
        Order $order
    ) {
        $aggregateId = AggregateId::fromString('id');
        $supplierId = 'supplier-id';
        $userId = UserId::fromString('user123');

        $order->getSupplierId()->willReturn($supplierId);

        $eventStoreRepository->findOrderById($aggregateId)->willReturn($order);
        $supplierRepository->find($supplierId)->willReturn(null);

        $this->shouldThrow(SupplierNotFoundException::class)->during('createOrderView', [$aggregateId, $userId]);
    }

    function it_creates_order_view(
        EventStoreRepository $eventStoreRepository,
        EntityRepository $supplierRepository,
        Order $order,
        FoodSupplier $foodSupplier
    ) {
        $aggregateId = AggregateId::fromString('id');
        $supplierId = 'supplier-id';
        $userId = UserId::fromString('user123');

        $order->getSupplierId()->willReturn($supplierId);

        $eventStoreRepository->findOrderById($aggregateId)->willReturn($order);
        $supplierRepository->find($supplierId)->willReturn($foodSupplier);

        $this->createOrderView($aggregateId, $userId)->shouldBeLike(new OrderView(
            $order->getWrappedObject(),
            $foodSupplier->getWrappedObject(),
            $userId
        ));
    }

    function it_creates_position_view(
        EventStoreRepository $eventStoreRepository,
        Order $order,
        Positions $positions,
        Position $position
    ) {
        $aggregateId = AggregateId::fromString('id');
        $userId = UserId::fromString('user123');

        $eventStoreRepository->findOrderById($aggregateId)->willReturn($order);
        $order->getPositions()->willReturn($positions);
        $positions->getLastAddedPosition()->willReturn($position);

        $this->createPositionView($aggregateId, $userId)->shouldBeLike(new PositionView(
            $position->getWrappedObject(),
            $userId
        ));
    }
}
