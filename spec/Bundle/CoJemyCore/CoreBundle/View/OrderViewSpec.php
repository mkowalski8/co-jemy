<?php

namespace spec\Bundle\CoJemyCore\CoreBundle\View;

use Application\View;
use Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier;
use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Order;
use Domain\CoJemy\Order\UserId;
use PhpSpec\ObjectBehavior;

class OrderViewSpec extends ObjectBehavior
{
    function it_returns_order_view_model_for_admin(
        Order $order,
        FoodSupplier $foodSupplier,
        Order\HashHolder $hashHolder
    ) {
        $aggregateId = AggregateId::generate();
        $adminHash = 'admin-hash';
        $userId = UserId::fromString('user123');

        $order->getHashHolder()->willReturn($hashHolder);
        $hashHolder->getAdminHash()->willReturn($adminHash);

        $order->getAggregateId()->willReturn($aggregateId);

        $order->getAdminIds()->willReturn([$userId]);

        $this->beConstructedWith($order, $foodSupplier, $userId);

        $this->shouldImplement(View::class);
        $this->toArray()->shouldBeLike([
            'isAdmin' => true,
            'adminHash' => $adminHash,
            'id' => (string) $aggregateId,
            'supplier' => $foodSupplier
        ]);
    }

    function it_returns_order_view_model_for_regular_user(
        Order $order,
        FoodSupplier $foodSupplier,
        Order\HashHolder $hashHolder
    ) {
        $aggregateId = AggregateId::generate();
        $adminHash = 'admin-hash';
        $userId = UserId::fromString('user123');

        $order->getHashHolder()->willReturn($hashHolder);
        $hashHolder->getAdminHash()->willReturn($adminHash);

        $order->getAggregateId()->willReturn($aggregateId);

        $order->getAdminIds()->willReturn([]);

        $this->beConstructedWith($order, $foodSupplier, $userId);

        $this->shouldImplement(View::class);
        $this->toArray()->shouldBeLike([
            'isAdmin' => false,
            'id' => (string) $aggregateId,
            'supplier' => $foodSupplier
        ]);
    }
}
