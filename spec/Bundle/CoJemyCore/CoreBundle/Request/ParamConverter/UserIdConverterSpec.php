<?php

namespace spec\Bundle\CoJemyCore\CoreBundle\Request\ParamConverter;

use Domain\CoJemy\Order\UserId;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

class UserIdConverterSpec extends ObjectBehavior
{
    function it_generates_random_user_id_when_no_user_id_is_specified_in_header(
        Request $request,
        ParameterBag $parameterBag,
        ParamConverter $paramConverter
    ) {
        $paramConverterName = 'param_converter_name';
        $paramConverter->getName()->willReturn($paramConverterName);

        $request->headers = new HeaderBag();

        $parameterBag->set($paramConverterName, Argument::type(UserId::class))->shouldBeCalled();
        $request->attributes = $parameterBag;

        $this->apply($request, $paramConverter);
    }

    function it_crates_user_id_from_string_in_header(
        Request $request,
        ParameterBag $parameterBag,
        ParamConverter $paramConverter
    ) {
        $paramConverterName = 'param_converter_name';
        $paramConverter->getName()->willReturn($paramConverterName);

        $request->headers = new HeaderBag([
            'userId' => 'some-user-id'
        ]);

        $parameterBag->set($paramConverterName, Argument::that(function($userId) {
            if ($userId instanceof UserId === true) {
                if ((string) $userId === 'some-user-id') {
                    return true;
                }
            }

            return false;
        }))->shouldBeCalled();
        $request->attributes = $parameterBag;

        $this->apply($request, $paramConverter);
    }
}
