<?php

namespace spec\Domain\CoJemy\Order;

use Domain\CoJemy\Order\Payments;
use Domain\CoJemy\Order\Payments\Payment;
use Domain\CoJemy\Order\Positions;
use PhpSpec\ObjectBehavior;

class PaymentsSpec extends ObjectBehavior
{
    function it_allows_to_add_payment()
    {
        $payment = Payment::withPositionId('some-id', 'some-name', 1000, 'PLN', 'position-id');
        $newPayments = $this->add($payment);
        $newPayments->shouldBeAnInstanceOf(Payments::class);
        $newPayments->count()->shouldReturn(1);

        $payment = Payment::withoutPositionId('some-other-id', 'some-name', 1000, 'PLN');
        $newPayments = $newPayments->add($payment);
        $newPayments->count()->shouldReturn(2);
    }

    function it_allows_to_remove_position_from_payment()
    {
        $payment = Payment::withPositionId('some-id', 'some-name', 1000, 'PLN', 'position-id');
        $newPayments = $this->add($payment);
        $newPayments->shouldBeAnInstanceOf(Payments::class);
        $newPayments->count()->shouldReturn(1);

        $newPayments = $newPayments->removePositionFromPayment($payment);
        $newPayments->toArray()[0]->getPositionId()->shouldBeLike(Positions\NullPositionId::generate());
    }

    function it_returns_payment_for_given_position_if_found()
    {
        $payment = Payment::withPositionId('some-id', 'some-name', 1000, 'PLN', 'position-id');
        $newPayments = $this->add($payment);

        $newPayments->findByPositionId(Positions\UniqIdPositionId::fromString('position-id'))->shouldReturn($payment);
    }

    function it_returns_null_when_payment_for_given_position_is_not_found()
    {
        $payment = Payment::withPositionId('some-id', 'some-name', 1000, 'PLN', 'position-id');
        $newPayments = $this->add($payment);

        $newPayments->findByPositionId(Positions\UniqIdPositionId::fromString('other-position-id'))->shouldReturn(null);
    }

    function it_returns_payments_as_array()
    {
        $payment = Payment::withPositionId('some-id', 'some-name', 1000, 'PLN', 'position-id');
        $newPayments = $this->add($payment);

        $newPayments->toArray()->shouldBeArray();
        $newPayments->toArray()->shouldHaveCount(1);
    }

    function it_creates_payments_with_items()
    {
        $payments = [
            Payment::withPositionId('some-id', 'some-name', 1000, 'PLN', 'position-id'),
            Payment::withPositionId('some-other-id', 'some-name', 1000, 'PLN', 'other-position-id'),
        ];

        $this->beConstructedThrough('createWithItems',[$payments]);
        $this->shouldBeAnInstanceOf(Payments::class);

        $this->count()->shouldReturn(2);

        $this->toArray()->shouldContain($payments[0]);
        $this->toArray()->shouldContain($payments[1]);
    }
}
