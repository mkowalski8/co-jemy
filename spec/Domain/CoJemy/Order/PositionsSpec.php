<?php

namespace spec\Domain\CoJemy\Order;

use Domain\CoJemy\Exception\Order\AccessDeniedException;
use Domain\CoJemy\Exception\Order\PositionNotFoundException;
use Domain\CoJemy\Order\Positions\Position;
use Domain\CoJemy\Order\Positions;
use Domain\CoJemy\Order\UserId;
use Domain\SharedKernel\Date;
use PhpSpec\ObjectBehavior;

class PositionsSpec extends ObjectBehavior
{
    function it_checks_if_position_exists()
    {
        $lastUpdate = (string) new Date();
        $position1 = new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', $lastUpdate, 'position1');
        $newPositions = $this->add($position1);
        $position2 = new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', $lastUpdate, 'position2');
        $newPositions = $newPositions->add($position2);

        $newPositions->exists(Positions\UniqIdPositionId::fromString('position1'))->shouldReturn(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position2'))->shouldBeLike(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position3'))->shouldBeLike(false);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position4'))->shouldBeLike(false);

    }

    function it_allows_to_add_position()
    {
        $lastUpdate = (string) new Date();

        $position = new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', $lastUpdate, 'position1');
        $newPositions = $this->add($position);
        $newPositions->shouldBeAnInstanceOf(Positions::class);
        $newPositions->count()->shouldReturn(1);

        $newPositions = $this->add($position);
        $position = new Position('user2', 'dish2', 'hamburger', 19.00, 'PLN', 'amanda', $lastUpdate, 'position2');
        $newPositions = $newPositions->add($position);

        $newPositions->shouldBeAnInstanceOf(Positions::class);
        $newPositions->count()->shouldReturn(2);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position1'))->shouldReturn(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position2'))->shouldReturn(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('not_existent_position'))->shouldReturn(false);
    }

    function it_allows_to_remove_position()
    {
        $lastUpdate = (string) new Date();

        $position1 = new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', $lastUpdate, 'position1');
        $newPositions = $this->add($position1);
        $position2 = new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', $lastUpdate, 'position2');
        $newPositions = $newPositions->add($position2);
        $position3 = new Position('user2', 'dish1', 'pizza', 23.00, 'PLN', 'john', $lastUpdate, 'position3');
        $newPositions = $newPositions->add($position3);

        $newPositions->count()->shouldReturn(3);

        $newPositions = $newPositions->remove(Positions\UniqIdPositionId::fromString('position1'), UserId::fromString('user1'));
        $newPositions->count()->shouldReturn(2);

        $newPositions->exists(Positions\UniqIdPositionId::fromString('position1'))->shouldReturn(false);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position2'))->shouldReturn(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position3'))->shouldReturn(true);

    }

    function it_does_not_allow_to_remove_position_owned_by_other_user()
    {
        $lastUpdate = (string) new Date();

        $newPositions = $this->add(new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', $lastUpdate, 'position1'));
        $newPositions = $newPositions->add(new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', $lastUpdate, 'position2'));
        $newPositions = $newPositions->add(new Position('user2', 'dish1', 'pizza', 23.00, 'PLN', 'john', $lastUpdate, 'position3'));

        $newPositions->count()->shouldReturn(3);

        $newPositions->shouldThrow(AccessDeniedException::class)->during('remove', [Positions\UniqIdPositionId::fromString('position3'), UserId::fromString('user1')]);
    }

    function it_allows_to_update_position()
    {
        $lastUpdate = (string) new Date();
        $newPositions = $this->add(new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', $lastUpdate, 'position1'));
        $newPositions = $newPositions->add(new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', $lastUpdate, 'position2'));
        $newPositions = $newPositions->add(new Position('user2', 'dish1', 'pizza', 23.00, 'PLN', 'john', $lastUpdate, 'position3'));

        $newPositions->count()->shouldReturn(3);

        $positionToUpdate = new Position('user1', 'dish2', 'pizza1', 43.00, 'PLN', 'andy', $lastUpdate, 'position1');

        $newPositions = $newPositions->update($positionToUpdate, UserId::fromString('user1'));
        $newPositions->count()->shouldReturn(3);

        $newPositions->find($positionToUpdate->getId())->shouldBeLike($positionToUpdate);
    }

    function it_does_not_allow_to_update_position_owned_by_other_user()
    {
        $lastUpdate = (string) new Date();

        $newPositions = $this->add(new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', $lastUpdate, 'position1'));
        $newPositions = $newPositions->add(new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', $lastUpdate, 'position2'));
        $newPositions = $newPositions->add(new Position('user2', 'dish1', 'pizza', 23.00, 'PLN', 'john', $lastUpdate, 'position3'));

        $newPositions->count()->shouldReturn(3);

        $positionToUpdate = new Position('user1', 'dish2', 'pizza1', 43.00, 'PLN', 'andy', $lastUpdate, 'position1');

        $newPositions->shouldThrow(AccessDeniedException::class)->during('update', [$positionToUpdate, UserId::fromString('user2')]);
    }

    function it_allows_to_check_if_user_can_remove_position()
    {
        $lastUpdate = (string) new Date();

        $newPositions = $this->add(new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', $lastUpdate, 'position1'));

        $newPositions->count()->shouldReturn(1);

        $newPositions->shouldNotThrow(AccessDeniedException::class)->during('checkModificationPossibility', [Positions\UniqIdPositionId::fromString('position1'), UserId::fromString('user1')]);
        $newPositions->shouldThrow(AccessDeniedException::class)->during('checkModificationPossibility', [Positions\UniqIdPositionId::fromString('position1'), UserId::fromString('user2')]);
        $newPositions->shouldThrow(AccessDeniedException::class)->during('checkModificationPossibility', [Positions\UniqIdPositionId::fromString('position1'), UserId::fromString('user3')]);
    }

    function it_returns_positions_as_array()
    {
        $lastUpdate = (string) new Date();

        $position1 = new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', $lastUpdate, 'position1');
        $newPositions = $this->add($position1);
        $position2 = new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', $lastUpdate, 'position2');
        $newPositions = $newPositions->add($position2);

        $newPositions->count()->shouldReturn(2);

        $newPositions->toArray()->shouldBeArray();
        $newPositions->toArray()->shouldHaveCount(2);
    }

    function it_creates_positions_with_items()
    {
        $lastUpdate = (string) new Date();

        $positions = [
            new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', $lastUpdate, 'position1'),
            new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', $lastUpdate, 'position2')
        ];

        $this->beConstructedThrough('createWithItems',[$positions]);

        $this->shouldBeAnInstanceOf(Positions::class);

        $this->count()->shouldReturn(2);

        $this->toArray()->shouldContain($positions[0]);
        $this->toArray()->shouldContain($positions[1]);
    }

    function it_returns_last_added_position_based_on_update_date()
    {
        $pos1Update = (string) new Date('-1 minute');
        $pos2Update = (string) new Date('now');

        $position1 = new Position('user1', 'dish1', 'pizza', 2300, 'PLN', 'andy', $pos1Update, 'position1');
        $newPositions = $this->add($position1);
        $position2 = new Position('user1', 'dish2', 'hamburger', 1900, 'PLN', 'andy', $pos2Update, 'position2');
        $newPositions = $newPositions->add($position2);

        $newPositions->getLastAddedPosition()->shouldReturn($position2);
    }

    function it_throws_exception_if_there_is_no_position()
    {
        $this->shouldThrow(PositionNotFoundException::class)->during('getLastAddedPosition');
    }
}
