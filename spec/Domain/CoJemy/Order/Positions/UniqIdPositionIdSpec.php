<?php

namespace spec\Domain\CoJemy\Order\Positions;

use PhpSpec\ObjectBehavior;

class UniqIdPositionIdSpec extends ObjectBehavior
{
    function it_generates_position_id()
    {
        $this->beConstructedThrough('generate');
        $this->getValue()->shouldBeString();
    }

    function it_creates_position_id_from_string()
    {
        $this->beConstructedThrough('fromString', ['any-value']);
        $this->getValue()->shouldReturn('any-value');
    }

    function it_returns_id_as_string()
    {
        $this->beConstructedThrough('fromString', ['any-value']);
        $this->__toString()->shouldReturn('any-value');
    }
}
