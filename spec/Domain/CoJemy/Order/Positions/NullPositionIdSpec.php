<?php

namespace spec\Domain\CoJemy\Order\Positions;

use PhpSpec\ObjectBehavior;

class NullPositionIdSpec extends ObjectBehavior
{
    function it_generates_null_position_id()
    {
        $this->beConstructedThrough('generate');
        $this->getValue()->shouldReturn(null);
    }

    function it_creates_null_position_id_from_string()
    {
        $this->beConstructedThrough('fromString', ['any-value']);
        $this->getValue()->shouldReturn(null);
    }
}
