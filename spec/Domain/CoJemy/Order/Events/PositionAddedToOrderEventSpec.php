<?php

namespace spec\Domain\CoJemy\Order\Events;

use PhpSpec\ObjectBehavior;
use Domain\CoJemy\Order\ParametersBag;
use Domain\CoJemy\Event;

class PositionAddedToOrderEventSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('order123', 'user123', 'dish123', 'burger', 999, 'PLN', 'mietek', 'position123', 'updateDate');
    }

    function it_returns_the_event_type()
    {
        $this->getType()->shouldReturn('PositionAddedToOrderEvent');
    }

    function it_is_an_event()
    {
        $this->shouldImplement(Event::class);
    }

    function it_returns_event_parameters()
    {
        $expectedParametersBag = new ParametersBag();
        $expectedParametersBag->setParameter('aggregateId', 'order123');
        $expectedParametersBag->setParameter('userId', 'user123');
        $expectedParametersBag->setParameter('dishId', 'dish123');
        $expectedParametersBag->setParameter('dishName', 'burger');
        $expectedParametersBag->setParameter('price', 999);
        $expectedParametersBag->setParameter('currency', 'PLN');
        $expectedParametersBag->setParameter('userNick', 'mietek');
        $expectedParametersBag->setParameter('positionId', 'position123');
        $expectedParametersBag->setParameter('lastUpdate', 'updateDate');

        $this->getParametersBag()->shouldBeLike($expectedParametersBag);
    }
}
