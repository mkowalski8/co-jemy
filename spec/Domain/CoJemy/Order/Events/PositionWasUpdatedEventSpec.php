<?php

namespace spec\Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;
use Domain\SharedKernel\Date;
use PhpSpec\ObjectBehavior;

class PositionWasUpdatedEventSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('order123', 'user123', 'dish123', 'burger', 999, 'PLN', 'mietek', 'now', 'position123');
    }

    function it_returns_the_event_type()
    {
        $this->getType()->shouldReturn('PositionWasUpdatedEvent');
    }

    function it_is_an_event()
    {
        $this->shouldImplement(Event::class);
    }

    function it_returns_event_parameters()
    {
        $expectedParametersBag = new ParametersBag();
        $expectedParametersBag->setParameter('aggregateId', 'order123');
        $expectedParametersBag->setParameter('userId', 'user123');
        $expectedParametersBag->setParameter('dishId', 'dish123');
        $expectedParametersBag->setParameter('dishName', 'burger');
        $expectedParametersBag->setParameter('price', 999);
        $expectedParametersBag->setParameter('currency', 'PLN');
        $expectedParametersBag->setParameter('userNick', 'mietek');
        $expectedParametersBag->setParameter('lastUpdate', 'now');
        $expectedParametersBag->setParameter('positionId', 'position123');

        $this->getParametersBag()->shouldBeLike($expectedParametersBag);
    }
}
