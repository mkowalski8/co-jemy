<?php

namespace spec\Domain\CoJemy\Order;

use Domain\CoJemy\Order\Status;
use PhpSpec\ObjectBehavior;

class StatusSpec extends ObjectBehavior
{
    function it_creates_opened_status()
    {
        $this->beConstructedThrough('opened');
        $this->__toString()->shouldReturn(Status::OPENED);
    }

    function it_creates_closed_status()
    {
        $this->beConstructedThrough('closed');
        $this->__toString()->shouldReturn(Status::CLOSED);
    }

    function it_is_closed() {
        $this->beConstructedThrough('closed');
        $this->isClosed()->shouldReturn(true);
    }
}
