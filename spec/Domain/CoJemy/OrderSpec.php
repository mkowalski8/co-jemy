<?php

namespace spec\Domain\CoJemy;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Exception\Order\AccessDeniedException;
use Domain\CoJemy\Exception\Order\PositionNotFoundException;
use Domain\CoJemy\Order\Dish;
use Domain\CoJemy\Order\Events\OrderClosedEvent;
use Domain\CoJemy\Order\Events\OrderOpenedEvent;
use Domain\CoJemy\Order\Events\PaymentAddedToOrderEvent;
use Domain\CoJemy\Order\Events\PositionAddedToOrderEvent;
use Domain\CoJemy\Order\Events\PositionWasUpdatedEvent;
use Domain\CoJemy\Order\Positions\NullPositionId;
use Domain\CoJemy\Order\Positions\Position;
use Domain\CoJemy\Order\Positions\UniqIdPositionId;
use Domain\CoJemy\Order\Prices;
use Domain\CoJemy\Order\Status;
use Domain\CoJemy\Order\HashHolder;
use Domain\CoJemy\Order\UserId;
use Domain\SharedKernel\Date;
use Domain\SharedKernel\Money;
use PhpSpec\ObjectBehavior;

class OrderSpec extends ObjectBehavior
{
    function it_opens_an_order()
    {
        $this->beConstructedWith(AggregateId::fromString('id123'));

        $adminId = UserId::fromString('admin123');

        $hashHolder = HashHolder::createFromHashes('adminHash123');
        $this->open('shama', $hashHolder, Money::fromString(1, 'PLN'), Money::fromString(2.5, 'PLN'), $adminId);

        $events = $this->getLatestEvents();
        $events->shouldHaveCount(1);
        $events[0]->shouldBeAnInstanceOf(OrderOpenedEvent::class);
        
        $expectedPrices = new Prices();
        $expectedPrices = $expectedPrices->addDeliveryCost(Money::fromString(2.5, 'PLN'));
        $expectedPrices = $expectedPrices->addPricePerPackage(Money::fromString(1, 'PLN'));
        $expectedPrices = $expectedPrices->addTotalAmount(Money::fromString(2.5, 'PLN'));

        $this->getAggregateId()->shouldBeLike(AggregateId::fromString('id123'));
        $this->getSupplierId()->shouldBe('shama');
        $this->getStatus()->shouldBeLike(Status::opened());
        $this->getPrices()->shouldBeLike($expectedPrices);
        $this->getHashHolder()->getAdminHash()->shouldBe('adminHash123');
        $this->getAdminIds()->shouldBeLike([$adminId]);
    }

    function it_recreates_order_from_events()
    {
        $hashHolder = HashHolder::createFromHashes('adminHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100, 250, 'PLN', 'admin123')
        ]]);

        $expectedPrices = new Prices();
        $expectedPrices = $expectedPrices->addDeliveryCost(Money::fromString(2.5, 'PLN'));
        $expectedPrices = $expectedPrices->addPricePerPackage(Money::fromString(1, 'PLN'));
        $expectedPrices = $expectedPrices->addTotalAmount(Money::fromString(2.5, 'PLN'));

        $this->getAggregateId()->shouldBeLike(AggregateId::fromString('id123'));
        $this->getSupplierId()->shouldBe('shama');
        $this->getStatus()->shouldBeLike(Status::opened());
        $this->getPrices()->shouldBeLike($expectedPrices);
        $this->getHashHolder()->getAdminHash()->shouldBe('adminHash123');
        $this->getAdminIds()->shouldBeLike([UserId::fromString('admin123')]);
    }

    function it_closes_an_order()
    {
        $hashHolder = HashHolder::createFromHashes('adminHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 10000, 20000, 'PLN', 'admin123')
        ]]);
        
        $this->close();

        $events = $this->getLatestEvents();
        $events->shouldHaveCount(1);
        $events[0]->shouldBeAnInstanceOf(OrderClosedEvent::class);
        
        $this->getAggregateId()->shouldBeLike(AggregateId::fromString('id123'));
        $this->getSupplierId()->shouldBe('shama');
        $this->getStatus()->shouldBeLike(Status::closed());
        $this->getAdminIds()->shouldBeLike([UserId::fromString('admin123')]);
    }

    function it_calculates_total_amount_when_adding_new_position()
    {
        $this->beConstructedWith(AggregateId::fromString('id123'));

        $hashHolder = HashHolder::createFromHashes('adminHash123');
        $adminId = UserId::fromString('admin123');
        $this->open('shama', $hashHolder, Money::fromString(0, 'PLN'), Money::fromString(0, 'PLN'), $adminId);
        $prices = $this->getPrices();
        $dish1 = Dish::withId('burger', 'dish1', Money::fromString(21.50, 'PLN'));
        $this->addPosition($dish1, 'user1', 'nick1', new Date());

        $expectedPrices = $prices->addTotalAmount(Money::fromString(21.50, 'PLN'));
        $this->getPrices()->shouldBeLike($expectedPrices);

        $dish2 = Dish::withId('mega burger', 'dish2', Money::fromString(35.00, 'PLN'));
        $this->addPosition($dish2, 'user2', 'nick2', new Date());

        $expectedPrices = $prices->addTotalAmount(Money::fromString(21.50+35.00, 'PLN'));
        $this->getPrices()->shouldBeLike($expectedPrices);
    }

    function it_calculates_total_amount_with_package_cost()
    {
        $this->beConstructedWith(AggregateId::fromString('id123'));

        $hashHolder = HashHolder::createFromHashes('adminHash123');
        $adminId = UserId::fromString('admin123');
        $this->open('shama', $hashHolder, Money::fromString(1, 'PLN'), Money::fromString(0, 'PLN'), $adminId);
        $prices = $this->getPrices();
        $dish1 = Dish::withId('burger', 'dish1', Money::fromString(21.50, 'PLN'));
        $dish2 = Dish::withId('mega burger', 'dish2', Money::fromString(35.00, 'PLN'));
        $this->addPosition($dish1, 'user1', 'nick1', new Date());
        $this->addPosition($dish2, 'user2', 'nick2', new Date());


        $expectedPrices = $prices->addTotalAmount(Money::fromString(21.50+1.00+35.00+1.00, 'PLN'));
        $this->getPrices()->shouldBeLike($expectedPrices);
    }

    function it_calculates_total_amount_with_delivery_cost()
    {
        $this->beConstructedWith(AggregateId::fromString('id123'));

        $hashHolder = HashHolder::createFromHashes('adminHash123');
        $adminId = UserId::fromString('admin123');
        $this->open('shama', $hashHolder, Money::fromString(0, 'PLN'), Money::fromString(5, 'PLN'), $adminId);
        $prices = $this->getPrices();
        $dish1 = Dish::withId('burger', 'dish1', Money::fromString(21.50, 'PLN'));
        $dish2 = Dish::withId('mega burger', 'dish2', Money::fromString(35.00, 'PLN'));
        $this->addPosition($dish1, 'user1', 'nick1', new Date());
        $this->addPosition($dish2, 'user2', 'nick2', new Date());


        $expectedPrices = $prices->addTotalAmount(Money::fromString(21.50+35.00+5, 'PLN'));
        $this->getPrices()->shouldBeLike($expectedPrices);
    }

    function it_removes_position_from_order()
    {
        $lastUpdate = (string) new Date();

        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100.00, 200.00, 'PLN', 'admin123'),
            new PositionAddedToOrderEvent('id123', 'user123', 'dish123', 'pancakes', 100.00, 'PLN', 'jack', 'position123', $lastUpdate),
            new PositionAddedToOrderEvent('id123', 'user124', 'dish124', 'hamburger', 200.00, 'PLN', 'andy', 'position124', $lastUpdate)
        ]]);

        $posId = UniqIdPositionId::fromString('position123');

        $this->removePosition($posId, UserId::fromString('user123'));
        $this->getPositions()->count()->shouldReturn(1);

        $this->getPositions()->exists($posId)->shouldReturn(false);
        $this->getPositions()->exists(UniqIdPositionId::fromString('position124'))->shouldReturn(true);
    }

    function it_removes_position_from_order_by_order_admin()
    {
        $lastUpdate = (string) new Date();

        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100.00, 200.00, 'PLN', 'admin123'),
            new PositionAddedToOrderEvent('id123', 'user123', 'dish123', 'pancakes', 100.00, 'PLN', 'jack', 'position123', $lastUpdate),
            new PositionAddedToOrderEvent('id123', 'user124', 'dish124', 'hamburger', 200.00, 'PLN', 'andy', 'position124', $lastUpdate)
        ]]);

        $posId = UniqIdPositionId::fromString('position123');

        $this->removePosition($posId, UserId::fromString('user123'), 'adminHash123');
        $this->getPositions()->count()->shouldReturn(1);
    }

    function it_throws_exception_when_unauthorized_user_try_to_remove_position()
    {
        $lastUpdate = (string) new Date();

        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100.00, 200.00, 'PLN', 'admin123'),
            new PositionAddedToOrderEvent('id123', 'user123', 'dish123', 'pancakes', 100.00, 'PLN', 'jack', 'position123', $lastUpdate),
            new PositionAddedToOrderEvent('id123', 'user124', 'dish124', 'hamburger', 200.00, 'PLN', 'andy', 'position124', $lastUpdate)
        ]]);

        $posId = UniqIdPositionId::fromString('position123');

        $this->shouldThrow(AccessDeniedException::class)->during('removePosition',[$posId, UserId::fromString('user124')]);
    }

    function it_throws_exception_when_user_try_to_remove_non_existing_position()
    {
        $lastUpdate = (string) new Date();

        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100.00, 200.00, 'PLN', 'admin123'),
            new PositionAddedToOrderEvent('id123', 'user123', 'dish123', 'pancakes', 100.00, 'PLN', 'jack', 'position123', $lastUpdate),
            new PositionAddedToOrderEvent('id123', 'user124', 'dish124', 'hamburger', 200.00, 'PLN', 'andy', 'position124', $lastUpdate)
        ]]);

        $posId = UniqIdPositionId::fromString('position125');

        $this->shouldThrow(PositionNotFoundException::class)->during('removePosition',[$posId, UserId::fromString('user123')]);
    }

    function it_adds_payment_to_order()
    {
        $lastUpdate = (string) new Date();

        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100.00, 200.00, 'PLN', 'admin123'),
            new PositionAddedToOrderEvent('id123', 'user123', 'dish123', 'pancakes', 100.00, 'PLN', 'jack', 'position123', $lastUpdate)
        ]]);

        $posId = UniqIdPositionId::fromString('position123');

        $this->addPositionPayment($posId, 'Jon Doe', Money::fromString(10, 'PLN'));

        $this->getPayments()->count()->shouldReturn(1);
        $this->getPayments()->toArray()[0]->getPositionId()->shouldBeLike($posId);
        $this->getPayments()->toArray()[0]->getPayerName()->shouldBe('Jon Doe');
        $this->getPayments()->toArray()[0]->getPaymentValue()->shouldBeLike(Money::fromString(10, 'PLN'));
    }

    function it_removes_position_id_from_payment_when_position_removed()
    {
        $lastUpdate = (string) new Date();

        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100.00, 200.00, 'PLN', 'admin123'),
            new PositionAddedToOrderEvent('id123', 'user123', 'dish123', 'pancakes', 100.00, 'PLN', 'jack', 'position123', $lastUpdate),
            new PaymentAddedToOrderEvent('id123', 'payment1', 'Jon Doe', 1000, 'PLN', 'position123')
        ]]);

        $posId = UniqIdPositionId::fromString('position123');

        $this->removePosition($posId, UserId::fromString('user123'));

        $this->getPayments()->count()->shouldReturn(1);
        $this->getPayments()->toArray()[0]->getPositionId()->shouldBeLike(NullPositionId::generate());
        $this->getPayments()->toArray()[0]->getPayerName()->shouldBe('Jon Doe');
        $this->getPayments()->toArray()[0]->getPaymentValue()->shouldBeLike(Money::fromString(10, 'PLN'));
    }

    function it_updates_position()
    {
        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        $lastUpdate = (string) new Date();

        $this->beConstructedThrough('recreate', ['id123', [
            new OrderOpenedEvent('id123', 'shama', $hashHolder->toArray(), 100, 200, 'PLN', 'admin123'),
            new PositionAddedToOrderEvent('id123', 'user123', 'dish123', 'pancakes', 100, 'PLN', 'jack', 'position123', $lastUpdate),
        ]]);

        $posId = UniqIdPositionId::fromString('position123');
        $dish = Dish::withId('my-dish', 'my-dish-id', Money::fromString(20, 'PLN'));
        $this->updatePosition($posId, $dish, UserId::fromString('user123'), 'John', new Date());

        $events = $this->getLatestEvents();
        $events->shouldHaveCount(1);
        $events[0]->shouldBeAnInstanceOf(PositionWasUpdatedEvent::class);

        $this->getPositions()->count()->shouldReturn(1);
        $this->getPositions()->toArray()[0]->shouldBeLike(
            new Position(
                'user123',
                'my-dish-id',
                'my-dish',
                2000,
                'PLN',
                'John',
                $lastUpdate,
                'position123'
            )
        );

        $this->getPrices()->getTotalAmount()->getAmount()->getAmount()->shouldBe(2300);
    }
}
