<?php

namespace spec\Domain\SharedKernel\Money;

use Domain\SharedKernel\Money\Currency;
use Domain\SharedKernel\Money\Exception\UnknownCurrencyException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CurrencySpec extends ObjectBehavior
{
    function it_throws_exception_when_created_with_unknown_currency()
    {
        $this->beConstructedWith('YYY');

        $this->shouldThrow(UnknownCurrencyException::class)
            ->duringInstantiation();
    }

    function it_can_be_initialized_for_known_currency()
    {
        $this->beConstructedWith('PLN');

        $this->shouldNotThrow()
            ->duringInstantiation();
    }

    function it_has_sub_unit()
    {
        $this->beConstructedWith('PLN');

        $this->getSubUnit()->shouldReturn(100);
    }

    function it_has_fraction_digits()
    {
        $this->beConstructedWith('PLN');

        $this->getFractionDigits()->shouldReturn(2);
    }

    function it_can_be_compared_to_another_currency()
    {
        $this->beConstructedWith('PLN');

        $equal = new Currency('PLN');

        $this->isEqualTo($equal)->shouldReturn(true);
    }
}
