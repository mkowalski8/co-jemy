<?php

namespace spec\Domain\SharedKernel;

use Domain\SharedKernel\Date;
use Domain\SharedKernel\Exception\InvalidDateFormatException;
use PhpSpec\ObjectBehavior;

class DateSpec extends ObjectBehavior
{
    function it_throws_exception_when_invalid_date_format_is_provided()
    {
        $date = 'invalid_date_format';

        $this->beConstructedWith($date);

        $this->shouldThrow(InvalidDateFormatException::class)->duringInstantiation();
    }

    function it_is_after_specified_date()
    {
        $date = 'now';
        $dateToCompare = new Date('-1 hour');

        $this->beConstructedWith($date);
        $this->isBefore($dateToCompare)->shouldReturn(false);
    }

    function it_is_before_specified_date()
    {
        $date = '-1 minute';
        $dateToCompare = new Date('now');

        $this->beConstructedWith($date);
        $this->isBefore($dateToCompare)->shouldReturn(true);
    }
}
