<?php

namespace Tests\Repository;

use Doctrine\ORM\EntityManager;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order;

use Bundle\CoJemyCore\CoreBundle\Entity\Event;
use Bundle\CoJemyCore\CoreBundle\Store\EventStore;

class DatabaseEventStore implements EventStorePersister, EventStoreRepository
{
    const ENTITY_PATH = 'CoreBundle:Event';

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /** @inheritdoc */
    public function persist(array $events)
    {
        $this->getEventStore()->persist($events);
    }

    /** @inheritdoc */
    public function findOrderById(AggregateId $id)
    {
        return $this->getEventStore()->findOrderById($id);
    }

    /**
     * @return int
     */
    public function count() : int
    {
        $events = $this
            ->getEventStore()
            ->createQueryBuilder('e')
            ->select('count(e)')
            ->groupBy('e.aggregateId')
            ->getQuery()
            ->getResult();

        return count($events);
    }

    /**
     * @return Order
     */
    public function getOrder() : Order
    {
        /** @var Event[] $events */
        $events = $this
            ->getEventStore()
            ->createQueryBuilder('e')
            ->getQuery()
            ->getResult();

        return $this->findOrderById(AggregateId::fromString($events[0]->getAggregateId()));
    }

    public function clear()
    {
        $this->entityManager->createQuery('DELETE ' . self::ENTITY_PATH)->execute();

        $metaData = $this->entityManager->getClassMetadata(self::ENTITY_PATH);
        $this->entityManager->getConnection()->exec('ALTER SEQUENCE ' . $metaData->getTableName() . '_id_seq RESTART WITH 1');

        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    /**
     * @return EventStore
     */
    private function getEventStore() : EventStore
    {
        return $this->entityManager->getRepository(Event::class);
    }
}
