<?php

namespace Tests\Builder;

use Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier;
use Bundle\CoJemyCore\CoreBundle\Entity\MenuItem;

class FoodSupplierBuilder
{
    private $id;
    private $name;
    private $deliveryCost;
    private $freeDeliveryThreshold;
    private $singlePackageCost;
    private $phoneNumber;
    private $websiteUrl;
    private $menuUrl;
    private $menuItems = [];
    
    public function __construct()
    {
        $this->id = 1;
        $this->name = 'supplier-1';
        $this->deliveryCost = '1000';
        $this->freeDeliveryThreshold = '5000';
        $this->singlePackageCost = '500';
        $this->phoneNumber = '123-456-789';
        $this->websiteUrl = 'http://supplier.com';
        $this->menuUrl = 'http://some-menu-url.pl';
    }

    /**
     * @return FoodSupplierBuilder
     */
    public function withMenuItem() : FoodSupplierBuilder
    {
        $this->menuUrl = null;

        $menuItem = new MenuItem();
        $menuItem->setName('item-1');
        $menuItem->setPrice('100.00');

        $this->menuItems[] = $menuItem;

        return $this;
    }

    /**
     * @param array $parameters
     *
     * @return FoodSupplierBuilder
     */
    public function withMenuItemWithParameters(array $parameters) : FoodSupplierBuilder
    {
        $menuItem = new MenuItem();
        $menuItem->setName($parameters['name']);
        $menuItem->setPrice($parameters['price']);

        $this->menuItems[] = $menuItem;

        return $this;
    }

    /**
     * @param FoodSupplier $foodSupplier
     *
     * @return FoodSupplier
     */
    public function buildMenuItemsForFoodSupplier(FoodSupplier $foodSupplier) : FoodSupplier
    {
        foreach ($this->menuItems as $menuItem) {
            $menuItem->setFoodSupplier($foodSupplier);
            $foodSupplier->addMenuItem($menuItem);
        }

        return $foodSupplier;
    }

    /**
     * @return FoodSupplier
     */
    public function build() : FoodSupplier
    {
        $foodSupplier = new FoodSupplier();
        $foodSupplier->setId($this->id);
        $foodSupplier->setName($this->name);
        $foodSupplier->setDeliveryCost($this->deliveryCost);
        $foodSupplier->setFreeDeliveryThreshold($this->freeDeliveryThreshold);
        $foodSupplier->setSinglePackageCost($this->singlePackageCost);
        $foodSupplier->setPhoneNumber($this->phoneNumber);
        $foodSupplier->setWebsiteUrl($this->websiteUrl);

        if (empty($this->menuItems)) {
            $foodSupplier->setMenuUrl($this->menuUrl);
        } else {
            foreach ($this->menuItems as $menuItem) {
                $menuItem->setFoodSupplier($foodSupplier);
                $foodSupplier->addMenuItem($menuItem);
            }
        }

        return $foodSupplier;
    }

    /**
     * @param array $parameters
     *
     * @return FoodSupplierBuilder
     */
    public function withParameters(array $parameters)
    {
        $this->id = $parameters['id'];
        $this->name = $parameters['name'];
        $this->deliveryCost = $parameters['deliveryCost'];
        $this->freeDeliveryThreshold = $parameters['freeDeliveryThreshold'];
        $this->singlePackageCost = $parameters['singlePackageCost'];
        $this->phoneNumber = $parameters['phoneNumber'];
        $this->websiteUrl = $parameters['websiteUrl'];
        $this->menuUrl = $parameters['menuUrl'];

        return $this;
    }
}
