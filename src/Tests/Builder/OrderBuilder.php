<?php

namespace Tests\Builder;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Order;
use Domain\CoJemy\Order\Dish;
use Domain\CoJemy\Order\OrderHashGenerator;
use Domain\CoJemy\Order\Positions\Position;
use Domain\CoJemy\Order\UserId;
use Domain\SharedKernel\Date;
use Domain\SharedKernel\Money;
use Domain\SharedKernel\Money\Currency;

class OrderBuilder
{
    const DEFAULT_SUPPLIER_ID = 'shama';
    const DEFAULT_AGGREGATE_ID = 'id123';
    const DEFAULT_PRICE_PACKAGE = 1000;
    const DEFAULT_DELIVERY_COST = 1000;
    const DEFAULT_ADMIN_ID = 'admin123';
    const DEFAULT_CURRENCY = 'PLN';

    /**
     * @var Order\HashHolder
     */
    private $hashHolder;

    /**
     * @var string
     */
    private $aggregateId;

    /**
     * @var string
     */
    private $supplierId;

    /**
     * @var Money
     */
    private $pricePackage;

    /**
     * @var Money
     */
    private $deliveryCost;

    /**
     * @var string
     */
    private $adminId;

    /**
     * @var Position[]
     */
    private $positions;

    /**
     * @var string
     */
    private $currency;

    public function __construct()
    {
        $hashGenerator = new OrderHashGenerator();

        $this->hashHolder = $hashGenerator->generate();
        $this->supplierId = self::DEFAULT_SUPPLIER_ID;
        $this->aggregateId = self::DEFAULT_AGGREGATE_ID;
        $this->pricePackage = new Money(self::DEFAULT_PRICE_PACKAGE, new Currency(self::DEFAULT_CURRENCY));
        $this->deliveryCost = new Money(self::DEFAULT_DELIVERY_COST, new Currency(self::DEFAULT_CURRENCY));
        $this->adminId = self::DEFAULT_ADMIN_ID;
        $this->positions = [];
        $this->currency = self::DEFAULT_CURRENCY;
    }

    /**
     * @param string $aggregateId
     * @return OrderBuilder
     */
    public function withAggregateId(string $aggregateId) : OrderBuilder
    {
        $this->aggregateId = $aggregateId;

        return $this;
    }

    /**
     * @param string $supplierId
     * @return OrderBuilder
     */
    public function withSupplier(string $supplierId) : OrderBuilder
    {
        $this->supplierId = $supplierId;

        return $this;
    }

    /**
     * @param int $pricePackage
     * @return OrderBuilder
     */
    public function withPricePackage(int $pricePackage) : OrderBuilder
    {
        $this->pricePackage = new Money($pricePackage, new Currency($this->currency));

        return $this;
    }

    /**
     * @param int $deliveryCost
     * @return OrderBuilder
     */
    public function withDeliveryCost(int $deliveryCost) : OrderBuilder
    {
        $this->deliveryCost = new Money($deliveryCost, new Currency($this->currency));

        return $this;
    }

    /**
     * @param string $adminId
     *
     * @return OrderBuilder
     */
    public function withAdminId(string $adminId) : OrderBuilder
    {
        $this->adminId = $adminId;

        return $this;
    }

    /**
     * @param array $parameters
     * @return OrderBuilder
     */
    public function withPosition(array $parameters) : OrderBuilder
    {
        $this->positions[] = $parameters;

        return $this;
    }

    /**
     * @return Order
     */
    public function build() : Order
    {
        $aggregateId = AggregateId::fromString($this->aggregateId);
        $adminId = UserId::fromString($this->adminId);
        $order = new Order($aggregateId);
        $order->open($this->supplierId, $this->hashHolder, $this->pricePackage, $this->deliveryCost, $adminId);
        if ($this->positions) {
            foreach ($this->positions as $position) {
                $dish = Dish::withId(
                    $position['dishName'],
                    $position['dishId'],
                    new Money($position['dishPrice'], new Currency($this->currency))
                );

                $order->addPosition(
                    $dish,
                    $position['userId'],
                    $position['userNick'],
                    new Date()
                );
            }
        }

        return $order;
    }
}
