<?php

namespace Domain\CoJemy;

interface EventStorePersister
{
    /**
     * @param Event[] $events
     */
    public function persist(array $events);
}
