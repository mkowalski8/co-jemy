<?php

namespace Domain\CoJemy\Aggregate;

class AggregateId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     */
    private function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return AggregateId
     */
    public static function generate() : AggregateId
    {
        return new self(uniqid());
    }

    /**
     * @param string $id
     * @return AggregateId
     */
    public static function fromString(string $id) : AggregateId
    {
        return new self($id);
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
       return $this->id;
    }
}
