<?php

namespace Domain\CoJemy;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Exception\Order\AccessDeniedException;
use Domain\CoJemy\Exception\Order\PositionNotFoundException;
use Domain\CoJemy\Order\Dish;
use Domain\CoJemy\Order\Events\OrderClosedEvent;
use Domain\CoJemy\Order\Events\OrderOpenedEvent;
use Domain\CoJemy\Order\Events\PaymentAddedToOrderEvent;
use Domain\CoJemy\Order\Events\PositionAddedToOrderEvent;
use Domain\CoJemy\Order\Events\PositionWasRemovedFromOrderEvent;
use Domain\CoJemy\Order\Events\PositionWasUpdatedEvent;
use Domain\CoJemy\Order\Payments;
use Domain\CoJemy\Order\Payments\Payment;
use Domain\CoJemy\Order\Payments\PaymentId;
use Domain\CoJemy\Order\Positions\Position;
use Domain\CoJemy\Order\Positions\UniqIdPositionId;
use Domain\CoJemy\Order\Positions\PositionId;
use Domain\CoJemy\Order\Positions;
use Domain\CoJemy\Order\Prices;
use Domain\CoJemy\Order\Status;
use Domain\CoJemy\Order\HashHolder;
use Domain\CoJemy\Order\UserId;
use Domain\SharedKernel\Date;
use Domain\SharedKernel\Money;

class Order extends Aggregate
{
    /**
     * @var string
     */
    private $supplierId;

    /**
     * @var Status
     */
    private $status;

    /**
     * @var HashHolder
     */
    private $hashHolder;

    /**
     * @var Prices
     */
    private $prices;

    /**
     * @var UserId[]
     */
    private $adminIds;

    /** @var Positions */
    private $positions;

    /** @var Payments */
    private $payments;

    /**
     * @param AggregateId $aggregateId
     */
    public function __construct(AggregateId $aggregateId)
    {
        parent::__construct($aggregateId);
        $this->adminIds = [];
        $this->positions = new Positions();
        $this->payments = new Payments();
    }

    /**
     * @param string $aggregateId
     * @param Event[] $events
     * @return Order
     */
    public static function recreate(string $aggregateId, array $events) : Order
    {
        $order = new self(AggregateId::fromString($aggregateId));

        foreach($events as $event) {
            $order->apply($event);
        }

        return $order;
    }

    /**
     * @param string $supplierId
     * @param HashHolder $hashHolder
     * @param Money $pricePerPackage
     * @param Money $deliverCost
     * @param UserId $adminId
     */
    public function open(
        string $supplierId,
        HashHolder $hashHolder,
        Money $pricePerPackage,
        Money $deliverCost,
        UserId $adminId
    ) {
        $event = new OrderOpenedEvent(
            (string) $this->id,
            (string) $supplierId,
            $hashHolder->toArray(),
            (int) $pricePerPackage->getAmount(),
            (int) $deliverCost->getAmount(),
            (string) $deliverCost->getCurrency(),
            (string) $adminId
        );
        $this->latestEvents[] = $event;
        $this->apply($event);
    }

    public function close()
    {
        $event = new OrderClosedEvent((string) $this->id);
        $this->latestEvents[] = $event;
        $this->apply($event);
    }

    /**
     * @param Dish $dish
     * @param string $userId
     * @param string $userNick
     * @param Date $lastUpdate
     */
    public function addPosition(Dish $dish, string $userId, string $userNick, Date $lastUpdate)
    {
        $price = $dish->getPrice()->getAmount();
        $event = new PositionAddedToOrderEvent(
            (string) $this->id,
            $userId,
            $dish->getId(),
            $dish->getName(),
            $price->getAmount(),
            (string) $price->getCurrency(),
            $userNick,
            (string) UniqIdPositionId::generate(),
            (string) $lastUpdate
        );
        $this->latestEvents[] = $event;
        $this->apply($event);
    }

    /**
     * @param PositionId $positionId
     * @param UserId $userId
     * @throws AccessDeniedException
     * @throws PositionNotFoundException
     */
    public function removePosition(PositionId $positionId, UserId $userId)
    {
        $this->positions->checkModificationPossibility($positionId, $userId);
        $event = new PositionWasRemovedFromOrderEvent((string) $this->id, (string) $userId, (string) $positionId);
        $this->latestEvents[] = $event;
        $this->apply($event);
    }

    /**
     * @param PositionId $positionId
     * @param Dish $dish
     * @param UserId $userId
     * @param string $userNick
     * @param Date $lastUpdate
     */
    public function updatePosition(PositionId $positionId, Dish $dish, UserId $userId, string $userNick, Date $lastUpdate)
    {
        $this->positions->checkModificationPossibility($positionId, $userId);
        $price = $dish->getPrice()->getAmount();
        $event = new PositionWasUpdatedEvent(
            (string) $this->id,
            (string) $userId,
            $dish->getId(),
            $dish->getName(),
            $price->getAmount(),
            (string) $price->getCurrency(),
            $userNick,
            (string) $lastUpdate,
            (string) $positionId
        );
        $this->latestEvents[] = $event;
        $this->apply($event);
    }

    /**
     * @param PositionId $positionId
     * @param string $payerName
     * @param Money $paymentValue
     */
    public function addPositionPayment(PositionId $positionId, string $payerName, Money $paymentValue)
    {
        $event = new PaymentAddedToOrderEvent(
            (string) $this->id,
            (string) PaymentId::generate(),
            $payerName,
            $paymentValue->getAmount(),
            (string) $paymentValue->getCurrency(),
            (string) $positionId
        );

        $this->latestEvents[] = $event;
        $this->apply($event);
    }
    
    /**
     * @return string
     */
    public function getSupplierId() : string
    {
        return $this->supplierId; 
    }

    /**
     * @return Status
     */
    public function getStatus() : Status
    {
        return $this->status;
    }

    /**
     * @return UserId[]
     */
    public function getAdminIds() : array
    {
        return $this->adminIds;
    }

    /**
     * @return Prices
     */
    public function getPrices() : Prices
    {
        return $this->prices;
    }

    /**
     * @return HashHolder
     */
    public function getHashHolder() : HashHolder
    {
        return $this->hashHolder;
    }

    /**
     * @return Positions
     */
    public function getPositions(): Positions
    {
        return $this->positions;
    }

    /**
     * @return Payments
     */
    public function getPayments(): Payments
    {
        return $this->payments;
    }

    /**
     * @param OrderOpenedEvent $event
     */
    protected function applyOrderOpenedEvent(OrderOpenedEvent $event)
    {
        $bag = $event->getParametersBag();

        $this->supplierId = $bag->getParameter('supplierId');

        $currency = new Money\Currency((string) $bag->getParameter('currency'));

        $this->prices = new Prices();
        $this->prices = $this->prices->addPricePerPackage(
            new Money((int) $bag->getParameter('pricePerPackage'), $currency)
        );
        $this->prices = $this->prices->addDeliveryCost(
            new Money((int) $bag->getParameter('deliveryCost'), $currency)
        );

        $this->prices = $this->prices->addTotalAmount($this->prices->getDeliveryCost()->getAmount());

        $hashes = $bag->getParameter('hashes');

        $this->hashHolder = HashHolder::createFromHashes($hashes['adminHash']);

        $this->adminIds[] = UserId::fromString($bag->getParameter('adminId'));

        $this->status = Status::opened();
    }

    /**
     * @param OrderClosedEvent $event
     */
    protected function applyOrderClosedEvent(OrderClosedEvent $event)
    {
        $this->status = Status::closed();
    }

    /**
     * @param PositionAddedToOrderEvent $event
     */
    protected function applyPositionAddedToOrderEvent(PositionAddedToOrderEvent $event)
    {
        $bag = $event->getParametersBag();

        $position = new Position(
            $bag->getParameter('userId'),
            $bag->getParameter('dishId'),
            $bag->getParameter('dishName'),
            $bag->getParameter('price'),
            $bag->getParameter('currency'),
            $bag->getParameter('userNick'),
            $bag->getParameter('lastUpdate'),
            $bag->getParameter('positionId')
        );

        $this->positions = $this->positions->add($position);

        $total = $this->prices->getTotalAmount()->getAmount();
        $pricePerPackage = $this->prices->getPricePerPackage()->getAmount();

        $this->prices = $this->prices->addTotalAmount(
            $total->add($position->getDishPrice())->add($pricePerPackage)
        );
    }

    /**
     * @param PositionWasUpdatedEvent $event
     */
    protected function applyPositionWasUpdatedEvent(PositionWasUpdatedEvent $event)
    {
        $bag = $event->getParametersBag();

        $position = new Position(
            $bag->getParameter('userId'),
            $bag->getParameter('dishId'),
            $bag->getParameter('dishName'),
            $bag->getParameter('price'),
            $bag->getParameter('currency'),
            $bag->getParameter('userNick'),
            $bag->getParameter('lastUpdate'),
            $bag->getParameter('positionId')
        );

        $previousPosition = $this->positions->find($position->getId());
        $this->positions = $this->positions->update($position, UserId::fromString($bag->getParameter('userId')));

        $total = $this->prices->getTotalAmount()->getAmount();
        $pricePerPackage = $this->prices->getPricePerPackage()->getAmount();

        $this->prices = $this->prices->addTotalAmount(
            $total
                ->subtract($pricePerPackage)
                ->subtract($previousPosition->getDishPrice())
                ->add($position->getDishPrice())
                ->add($pricePerPackage)
        );
    }

    /**
     * @param PositionWasRemovedFromOrderEvent $event
     * @throws Exception\Order\ParametersBag\UnknownParameterNameException
     */
    protected function applyPositionWasRemovedFromOrderEvent(PositionWasRemovedFromOrderEvent $event)
    {
        $bag = $event->getParametersBag();
        $positionId = UniqIdPositionId::fromString($bag->getParameter('positionId'));
        $position = $this->positions->find($positionId);

        $this->positions = $this->positions->remove(
            $positionId,
            UserId::fromString($bag->getParameter('userId'))
        );

        $positionPayment = $this->payments->findByPositionId($positionId);
        if ($positionPayment) {
            $this->payments = $this->payments->removePositionFromPayment($positionPayment);
        }

        $total = $this->prices->getTotalAmount()->getAmount();
        $pricePerPackage = $this->prices->getPricePerPackage()->getAmount();

        $this->prices = $this->prices->addTotalAmount(
            $total
                ->subtract($pricePerPackage)
                ->subtract($position->getDishPrice())
        );
    }

    /**
     * @param PaymentAddedToOrderEvent $event
     */
    protected function applyPaymentAddedToOrderEvent(PaymentAddedToOrderEvent $event)
    {
        $bag = $event->getParametersBag();

        $payment = Payment::withPositionId(
            $bag->getParameter('paymentId'),
            $bag->getParameter('payerName'),
            $bag->getParameter('paymentAmount'),
            $bag->getParameter('paymentCurrency'),
            $bag->getParameter('positionId')
        );

        $this->payments = $this->payments->add($payment);
    }
}
