<?php

namespace Domain\CoJemy\Order;

class HashHolder
{
    /**
     * @var string
     */
    private $adminHash;

    /**
     * @param string $adminHash
     */
    private function __construct(string $adminHash)
    {
        $this->adminHash = $adminHash;
    }

    public static function createFromHashes(string $adminHash) : HashHolder
    {
        return new HashHolder($adminHash);
    }

    /**
     * @return mixed
     */
    public function getAdminHash() : string
    {
        return $this->adminHash;
    }

    public function toArray() : array
    {
        return [
            'adminHash' => $this->getAdminHash()
        ];
    }
}
