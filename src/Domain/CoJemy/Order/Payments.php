<?php

namespace Domain\CoJemy\Order;

use Domain\CoJemy\Order\Payments\Payment;
use Domain\CoJemy\Order\Positions\PositionId;

class Payments
{
    /**
     * @var Payment[]
     */
    private $items;

    /**
     * @param array $items
     * @return Payments
     */
    public static function createWithItems(array $items)
    {
        $payments = new self();
        $payments->items = $items;

        return $payments;
    }

    /**
     * Payments constructor.
     */
    public function __construct()
    {
        $this->items = [];
    }

    /**
     * @param Payment $payment
     * @return Payments
     */
    public function add(Payment $payment) : Payments
    {
        $items = $this->items;
        $items[(string) $payment->getId()] = $payment;

        return self::createWithItems($items);
    }

    /**
     * Count number of items
     *
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * Return collection as array
     *
     * @return array
     */
    public function toArray() : array
    {
        return array_values($this->items);
    }

    /**
     * @param Payment $payment
     * @return Payments
     */
    public function removePositionFromPayment(Payment $payment)
    {
        $payments = array_filter($this->items, function(Payment $item) use ($payment) {
            return !$item->isEqualTo($payment);
        });

        $payments[] = Payment::withoutPositionId(
            (string) $payment->getId(),
            $payment->getPayerName(),
            $payment->getPaymentValue()->getAmount(),
            (string) $payment->getPaymentValue()->getCurrency()
        );

        return self::createWithItems($payments);
    }

    /**
     * @param PositionId $positionId
     * @return bool|null
     */
    public function findByPositionId(PositionId $positionId)
    {
        foreach ($this->items as $item) {
            if ($item->getPositionId()->isEqualTo($positionId)) {
                return $item;
            }
        }

        return null;
    }
}

