<?php

namespace Domain\CoJemy\Order;

use Domain\CoJemy\Exception\Order\AccessDeniedException;
use Domain\CoJemy\Exception\Order\PositionNotFoundException;
use Domain\CoJemy\Order\Positions\Position;
use Domain\CoJemy\Order\Positions\PositionId;
use Domain\SharedKernel\Date;

class Positions
{
    /**
     * @var Position[]
     */
    private $items;

    /**
     * Create Positions with items
     *
     * @param array $items
     * @return Positions
     */
    public static function createWithItems(array $items)
    {
        $positions = new self();
        $positions->items = $items;

        return $positions;
    }

    /**
     * Positions constructor.
     */
    public function __construct()
    {
        $this->items = [];
    }

    /**
     * Count number of items
     *
     * @return int
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * Add position to collection
     *
     * @param Position $position
     * @return Positions
     */
    public function add(Position $position) : Positions
    {
        $items = $this->items;
        $items[(string) $position->getId()] = $position;

        return self::createWithItems($items);
    }

    /**
     * Update position
     *
     * @param Position $position
     * @param UserId $userId
     * @return Positions
     */
    public function update(Position $position, UserId $userId) : Positions
    {
        $this->checkModificationPossibility($position->getId(), $userId);

        $items = $this->items;
        $items[(string) $position->getId()] = $position;

        return self::createWithItems($items);
    }

    /**
     * Remove position from collection
     *
     * @param PositionId $positionId
     * @param UserId $userId
     * @return Positions
     * @throws AccessDeniedException
     * @throws PositionNotFoundException
     */
    public function remove(PositionId $positionId, UserId $userId) : Positions
    {
        $this->checkModificationPossibility($positionId, $userId);

        $items = $this->items;
        unset($items[(string) $positionId]);
        return self::createWithItems($items);
    }

    /**
     * Check if position exists
     *
     * @param PositionId $positionId
     * @return bool
     */
    public function exists(PositionId $positionId) : bool
    {
        if (!empty($this->items[(string) $positionId])) {
            return true;
        }

        return false;
    }

    /**
     * Check if specific user can remove particular position
     *
     * @param PositionId $positionId
     * @param UserId $userId
     * @throws AccessDeniedException
     * @throws PositionNotFoundException
     */
    public function checkModificationPossibility(PositionId $positionId, UserId $userId)
    {
        if (!$position = $this->find($positionId)) {
            throw new PositionNotFoundException(sprintf('Position identified by %s does not exist', (string) $positionId));
        }
        $this->ownerGuard($position, $userId);
    }

    /**
     * Return collection as array
     *
     * @return array
     */
    public function toArray() : array
    {
        return array_values($this->items);
    }

    /**
     * Position finder
     *
     * @param PositionId $positionId
     * @return Position|null
     * @throws PositionNotFoundException
     */
    public function find(PositionId $positionId)
    {
        if ($this->exists($positionId)) {
            return $this->items[(string)$positionId];
        }

        return null;
    }

    /**
     * Ownership guard
     *
     * @param Position $position
     * @param UserId $userId
     * @throws AccessDeniedException
     */
    private function ownerGuard(Position $position, UserId $userId)
    {
        // TODO allow to remove by admin hash holder
        if ($position->getUserId() != $userId) {
            throw new AccessDeniedException(sprintf("User identified by '%s' does not owned '%s'", (string) $userId, (string) $position->getId()));
        }
    }

    /**
     * @return Position
     * @throws PositionNotFoundException
     */
    public function getLastAddedPosition()
    {
        if (!$this->count()) {
            throw new PositionNotFoundException('Order does not have any position');
        }

        $itemToCompare = $this->toArray()[0];
        foreach ($this->items as $item) {
            $itemToCompare = $this->getLastModifiedItem($item, $itemToCompare);
        }

        return $itemToCompare;
    }

    /**
     * @param Position $item
     * @param Position $itemToCompare
     *
     * @return Position
     */
    private function getLastModifiedItem(Position $item, Position $itemToCompare)
    {
        if ($item->getLastUpdate()->isBefore($itemToCompare->getLastUpdate())) {
            return $itemToCompare;
        }

        return $item;
    }
}

