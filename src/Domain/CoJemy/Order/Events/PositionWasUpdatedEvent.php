<?php

namespace Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;

class PositionWasUpdatedEvent implements Event
{
    /**
     * @var string
     */
    private $aggregateId;

    /**
     * @var string
     */
    private $positionId;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $dishId;

    /**
     * @var string
     */
    private $dishName;

    /**
     * @var int
     */
    private $price;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $lastUpdate;

    /**
     * @var string
     */
    private $userNick;

    /**
     * @param string $aggregateId
     * @param string $userId
     * @param string $dishId
     * @param string $dishName
     * @param int $price
     * @param string $currency
     * @param string $userNick
     * @param string $lastUpdate
     * @param string $positionId
     */
    public function __construct(
        string $aggregateId,
        string $userId,
        string $dishId,
        string $dishName,
        int $price,
        string $currency,
        string $userNick,
        string $lastUpdate,
        string $positionId
    ) {
        $this->aggregateId = $aggregateId;
        $this->userId = $userId;
        $this->dishId = $dishId;
        $this->dishName = $dishName;
        $this->price = $price;
        $this->currency = $currency;
        $this->userNick = $userNick;
        $this->lastUpdate = $lastUpdate;
        $this->positionId = $positionId;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return 'PositionWasUpdatedEvent';
    }

    /**
     * @return ParametersBag
     */
    public function getParametersBag() : ParametersBag
    {
        $parameters = new ParametersBag();

        $parameters->setParameter('aggregateId', $this->aggregateId);
        $parameters->setParameter('userId', $this->userId);
        $parameters->setParameter('dishId', $this->dishId);
        $parameters->setParameter('dishName', $this->dishName);
        $parameters->setParameter('price', $this->price);
        $parameters->setParameter('currency', $this->currency);
        $parameters->setParameter('userNick', $this->userNick);
        $parameters->setParameter('lastUpdate', $this->lastUpdate);
        $parameters->setParameter('positionId', $this->positionId);

        return $parameters;
    }

    /**
     * @param array $parameters
     *
     * @return PositionWasUpdatedEvent
     */
    public static function fromParameters(array $parameters) : PositionWasUpdatedEvent
    {
        return new self(
            $parameters['aggregateId'],
            $parameters['userId'],
            $parameters['dishId'],
            $parameters['dishName'],
            $parameters['price'],
            $parameters['currency'],
            $parameters['userNick'],
            $parameters['lastUpdate'],
            $parameters['positionId']
        );
    }
}
