<?php

namespace Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;

class PositionAddedToOrderEvent implements Event
{
    /**
     * @var string
     */
    private $aggregateId;

    /**
     * @var string
     */
    private $positionId;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $dishId;

    /**
     * @var string
     */
    private $dishName;

    /**
     * @var int
     */
    private $price;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $userNick;

    /**
     * @var string
     */
    private $lastUpdate;

    /**
     * @param string $aggregateId
     * @param string $userId
     * @param string $dishId
     * @param string $dishName
     * @param int $price
     * @param string $currency
     * @param string $userNick
     * @param string $positionId
     * @param string $lastUpdate
     */
    public function __construct(
        string $aggregateId,
        string $userId,
        string $dishId,
        string $dishName,
        int $price,
        string $currency,
        string $userNick,
        string $positionId,
        string $lastUpdate
    ) {
        $this->aggregateId = $aggregateId;
        $this->userId = $userId;
        $this->dishId = $dishId;
        $this->dishName = $dishName;
        $this->price = $price;
        $this->currency = $currency;
        $this->userNick = $userNick;
        $this->positionId = $positionId;
        $this->lastUpdate = $lastUpdate;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return 'PositionAddedToOrderEvent';
    }

    /**
     * @return ParametersBag
     */
    public function getParametersBag() : ParametersBag
    {
        $parameters = new ParametersBag();

        $parameters->setParameter('aggregateId', $this->aggregateId);
        $parameters->setParameter('userId', $this->userId);
        $parameters->setParameter('dishId', $this->dishId);
        $parameters->setParameter('dishName', $this->dishName);
        $parameters->setParameter('price', $this->price);
        $parameters->setParameter('currency', $this->currency);
        $parameters->setParameter('userNick', $this->userNick);
        $parameters->setParameter('positionId', $this->positionId);
        $parameters->setParameter('lastUpdate', $this->lastUpdate);

        return $parameters;
    }

    /**
     * @param array $parameters
     *
     * @return PositionAddedToOrderEvent
     */
    public static function fromParameters(array $parameters) : PositionAddedToOrderEvent
    {
        return new self(
            $parameters['aggregateId'],
            $parameters['userId'],
            $parameters['dishId'],
            $parameters['dishName'],
            $parameters['price'],
            $parameters['currency'],
            $parameters['userNick'],
            $parameters['positionId'],
            $parameters['lastUpdate']
        );
    }
}
