<?php

namespace Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;

class PositionWasRemovedFromOrderEvent implements Event
{
    private $aggregateId;
    private $userId;
    private $positionId;

    public function __construct(string $aggregateId, string $userId, string $positionId)
    {
        $this->aggregateId = $aggregateId;
        $this->userId = $userId;
        $this->positionId = $positionId;
    }

    public function getType() : string
    {
        return 'PositionWasRemovedFromOrderEvent';
    }

    public function getParametersBag() : ParametersBag
    {
        $parameters = new ParametersBag();

        $parameters->setParameter('aggregateId', $this->aggregateId);
        $parameters->setParameter('userId', $this->userId);
        $parameters->setParameter('positionId', $this->positionId);
        return $parameters;
    }

    /**
     * @param array $parameters
     *
     * @return PositionWasRemovedFromOrderEvent
     */
    public static function fromParameters(array $parameters) : PositionWasRemovedFromOrderEvent
    {
        return new self(
            $parameters['aggregateId'],
            $parameters['userId'],
            $parameters['positionId']
        );
    }
}
