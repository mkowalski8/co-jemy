<?php

namespace Domain\CoJemy\Order\Positions;

class NullPositionId implements PositionId
{
    /**
     * @param string $id
     */
    private function __construct(){}

    /**
     * @return PositionId
     */
    public static function generate() : PositionId
    {
        return new self();
    }

    /**
     * @param string $id
     * @return PositionId
     */
    public static function fromString(string $id) : PositionId
    {
        return new self();
    }

    /**
     * @return string|null
     */
    public function getValue()
    {
        return null;
    }

    /**
     * @param PositionId $positionId
     * @return bool
     */
    public function isEqualTo(PositionId $positionId)
    {
        return null === $positionId->getValue();
    }
}
