<?php

namespace Domain\CoJemy\Order\Positions;

use Domain\CoJemy\Order\Dish;
use Domain\CoJemy\Order\UserId;
use Domain\SharedKernel\Date;
use Domain\SharedKernel\Money;

class Position
{
    /** @var  UserId */
    private $userId;

    /** @var  Dish */
    private $dish;

    /** @var  string */
    private $userNick;

    /** @var  Date */
    private $lastUpdate;

    /** @var UniqIdPositionId */
    private $id;

    /**
     * Position constructor.
     * @param string $userId
     * @param string $dishId
     * @param string $dishName
     * @param int $priceAmount
     * @param string $priceCurrency
     * @param string $userNick
     * @param string $lastUpdate
     * @param string|null $id
     */
    public function __construct(
        string $userId,
        string $dishId,
        string $dishName,
        int $priceAmount,
        string $priceCurrency,
        string $userNick,
        string $lastUpdate,
        string $id = null
    ) {
        $price = new Money($priceAmount, new Money\Currency($priceCurrency));

        $this->userId = UserId::fromString($userId);
        $this->dish = $dishId ? Dish::withId($dishName, $dishId, $price) : Dish::withoutId($dishName, $price);
        $this->userNick = $userNick;
        $this->id = $id ? UniqIdPositionId::fromString($id) : UniqIdPositionId::generate();
        $this->lastUpdate = new Date($lastUpdate);
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }

    /**
     * @return Dish
     */
    public function getDish() : Dish
    {
        return $this->dish;
    }

    /**
     * @return string
     */
    public function getUserNick() : string
    {
        return $this->userNick;
    }

    /**
     * @return UniqIdPositionId
     */
    public function getId() : UniqIdPositionId
    {
        return $this->id;
    }

    /**
     * @return Date
     */
    public function getLastUpdate() : Date
    {
        return $this->lastUpdate;
    }

    /**
     * @return Money
     */
    public function getDishPrice(): Money
    {
        return $this->dish->getPrice()->getAmount();
    }
}
