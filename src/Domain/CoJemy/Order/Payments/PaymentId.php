<?php

namespace Domain\CoJemy\Order\Payments;

class PaymentId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     */
    private function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return PaymentId
     */
    public static function generate() : PaymentId
    {
        return new self(uniqid());
    }

    /**
     * @param string $id
     * @return PaymentId
     */
    public static function fromString(string $id) : PaymentId
    {
        return new self($id);
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->id;
    }
}
