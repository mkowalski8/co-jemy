<?php

namespace Domain\CoJemy\Order;

class OrderHashGenerator
{
    /**
     * @return HashHolder
     */
    public function generate() : HashHolder
    {
        $hashHolder = HashHolder::createFromHashes(md5(uniqid('admin', true)));

        return $hashHolder;
    }
}
