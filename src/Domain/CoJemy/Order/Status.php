<?php

namespace Domain\CoJemy\Order;

class Status
{
    const OPENED = 'opened';
    const CLOSED = 'closed';

    /**
     * @var string
     */
    private $status;

    /**
     * @param string $statusName
     */
    private function __construct(string $statusName)
    {
        $this->status = $statusName;
    }

    /**
     * @return Status
     */
    static public function opened() : Status
    {
        return new self(self::OPENED);
    }

    /**
     * @return Status
     */
    static public function closed() : Status
    {
        return new self(self::CLOSED);
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isClosed() : bool
    {
        return $this->status === self::CLOSED;
    }
}
