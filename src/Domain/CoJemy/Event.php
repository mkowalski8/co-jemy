<?php

namespace Domain\CoJemy;

use Domain\CoJemy\Order\ParametersBag;

interface Event
{
    /**
     * @return string
     */
    public function getType() : string;

    /**
     * @return ParametersBag
     */
    public function getParametersBag() : ParametersBag;
}
