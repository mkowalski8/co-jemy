<?php

namespace Domain\CoJemy;

use Domain\CoJemy\Aggregate\AggregateId;

abstract class Aggregate
{
    /**
     * @var AggregateId
     */
    protected $id;

    /**
     * @var Event[]
     */
    protected $latestEvents = [];

    /**
     * @param AggregateId $id
     */
    public function __construct(AggregateId $id)
    {
        $this->id = $id;
    }

    /**
     * @return Event[]
     */
    public function getLatestEvents() : array
    {
        $events = $this->latestEvents;
        $this->latestEvents = [];

        return $events;
    }

    public function getAggregateId() : AggregateId
    {
        return $this->id;
    }

    /**
     * @param Event $event
     */
    protected function apply(Event $event)
    {
        $methodName = 'apply'.$event->getType();
        $this->$methodName($event);
    }
}
