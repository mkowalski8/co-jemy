<?php

namespace Domain\CoJemy;

use Domain\CoJemy\Order\Prices\Price;
use Domain\CoJemy\Order\Prices\Type;
use Domain\SharedKernel\Money;

final class Supplier
{
    private $id;

    private $pricePerPackage;

    private $freeDeliveryThreshold;

    private $deliveryCost;

    /**
     * @param string $id
     * @param Money $pricePerPackage
     * @param Money $freeDeliveryThreshold
     * @param Money $deliveryCost
     */
    public function __construct(string $id, Money $pricePerPackage, Money $freeDeliveryThreshold, Money $deliveryCost)
    {
        $this->id = $id;
        $this->pricePerPackage = new Price(Type::pricePerPackage(), $pricePerPackage);
        $this->freeDeliveryThreshold = $freeDeliveryThreshold;
        $this->deliveryCost = new Price(Type::deliveryCost(), $deliveryCost);
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function getPricePerPackage() : Price
    {
        return $this->pricePerPackage;
    }

    public function getDeliveryCost() : Price
    {
        return $this->deliveryCost;
    }

    public function getFreeDeliveryThreshold() : Money
    {
        return $this->freeDeliveryThreshold;
    }
}
