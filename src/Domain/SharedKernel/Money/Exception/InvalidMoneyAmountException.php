<?php

namespace Domain\SharedKernel\Money\Exception;

class InvalidMoneyAmountException extends \Exception
{
}
