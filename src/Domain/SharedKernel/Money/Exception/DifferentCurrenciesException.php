<?php

namespace Domain\SharedKernel\Money\Exception;

class DifferentCurrenciesException extends \Exception
{
}
