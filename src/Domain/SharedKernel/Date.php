<?php
/**
 * Created by PhpStorm.
 * User: mszoltysek
 * Date: 28.06.2016
 * Time: 14:37
 */

namespace Domain\SharedKernel;

use Domain\SharedKernel\Exception\InvalidDateFormatException;

class Date
{
    /**
     * @var \DateTimeImmutable
     */
    private $date;

    /**
     * Date constructor.
     * @param string $dateString - initial date to set
     */
    public function __construct(string $dateString = 'now')
    {
        $this->guardCorrectDate($dateString);

        $this->date = new \DateTimeImmutable($dateString);
    }

    /**
     * @param $dateString
     * @throws InvalidDateFormatException
     */
    private function guardCorrectDate(string $dateString)
    {
        if (false === $timestamp = strtotime($dateString)) {
            throw new InvalidDateFormatException(sprintf('Date string %s is invalid', $dateString));
        }
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->date->format("Y-m-d H:i:s");
    }

    /**
     * @param Date $dateToCompare
     * @return bool
     */
    public function isBefore(Date $dateToCompare) : bool
    {
        return $this->date < $dateToCompare->date;
    }
}
