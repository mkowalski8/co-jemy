<?php

namespace Domain\SharedKernel;

use Domain\SharedKernel\Money\Currency;
use Domain\SharedKernel\Money\Exception\DifferentCurrenciesException;
use Domain\SharedKernel\Money\Exception\InvalidMoneyAmountException;

class Money
{
    /**
     * @var int
     */
    private $amount;

    /**
     * @var Currency
     */
    private $currency;

    public function __construct(int $amount, Currency $currency)
    {
        if ($amount < 0) {
            throw new InvalidMoneyAmountException('Money amount must be positive number');
        }

        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * Based on https://github.com/sebastianbergmann/money/blob/v1.5.1/src/Money.php#L77
     *
     * @param string $amount
     * @param string $currencyCode
     * @return Money
     */
    public static function fromString(string $amount, string $currencyCode) : self
    {
        $currency = new Currency($currencyCode);

        $intAmount = (int) round(
            $currency->getSubUnit() *
            round(
                $amount,
                $currency->getFractionDigits(),
                PHP_ROUND_HALF_UP
            ),
            0,
            PHP_ROUND_HALF_UP
        );

        return new self($intAmount, $currency);
    }

    public function getAmount() : int
    {
        return $this->amount;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @param Money $other
     * @return bool
     */
    public function isEqualTo(self $other) : bool
    {
        return $this->amount === $other->amount && $this->currency->isEqualTo($other->currency);
    }

    /**
     * @param Money $other
     * @return Money
     *
     * @throws DifferentCurrenciesException
     */
    public function add(self $other) : self
    {
        $this->guardDifferentCurrencies($other);

        return new self($this->amount + $other->amount, $this->currency);
    }

    /**
     * @param Money $other
     * @return Money
     *
     * @throws DifferentCurrenciesException
     */
    public function subtract(self $other) : self
    {
        $this->guardDifferentCurrencies($other);
        
        return new self($this->amount - $other->amount, $this->currency);
    }

    public function __toString() : string
    {
        return (float) $this->amount / $this->currency->getSubUnit() . ' ' . (string) $this->currency;
    }

    private function guardDifferentCurrencies(self $money)
    {
        if (!$this->currency->isEqualTo($money->currency)) {
            throw new DifferentCurrenciesException(
                sprintf('Currencies mismatch: "%s" != "%s"', $this->currency, $money->currency)
            );
        }
    }
}
