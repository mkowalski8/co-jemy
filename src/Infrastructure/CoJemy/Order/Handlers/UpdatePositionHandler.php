<?php

namespace Infrastructure\CoJemy\Order\Handlers;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order\Dish;
use Domain\CoJemy\Order\Positions\UniqIdPositionId;
use Domain\CoJemy\Order\UserId;
use Domain\SharedKernel\Date;
use Domain\SharedKernel\Money;
use Domain\SharedKernel\Money\Currency;
use Infrastructure\CoJemy\Order\Commands\UpdatePositionCommand;

class UpdatePositionHandler
{
    /**
     * @var EventStoreRepository
     */
    private $eventStoreRepository;

    /**
     * @var EventStorePersister
     */
    private $eventStorePersister;

    /**
     * @param EventStoreRepository $eventStoreRepository
     * @param EventStorePersister $eventStorePersister
     */
    public function __construct(EventStoreRepository $eventStoreRepository, EventStorePersister $eventStorePersister)
    {
        $this->eventStoreRepository = $eventStoreRepository;
        $this->eventStorePersister = $eventStorePersister;
    }

    /**
     * @param UpdatePositionCommand $command
     */
    public function handleUpdatePositionCommand(UpdatePositionCommand $command)
    {
        $order = $this->eventStoreRepository->findOrderById(AggregateId::fromString($command->getOrderId()));
        $userId = $command->getUserId() ? UserId::fromString($command->getUserId()) : UserId::generate();
        $dish = Dish::withId(
            $command->getDishName(),
            $command->getDishId(),
            new Money($command->getPrice(), new Currency($command->getCurrency()))
        );
        $order->updatePosition(
            UniqIdPositionId::fromString($command->getPositionId()),
            $dish,
            $userId,
            $command->getUserNick(),
            new Date()
        );

        $this->eventStorePersister->persist($order->getLatestEvents());
    }
}
