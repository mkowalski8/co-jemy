<?php

namespace Infrastructure\CoJemy\Order\Handlers;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order\Positions\UniqIdPositionId;
use Domain\SharedKernel\Money;
use Infrastructure\CoJemy\Order\Commands\AddPaymentToOrderCommand;

class AddPaymentToOrderHandler
{
    /**
     * @var EventStoreRepository
     */
    private $eventStoreRepository;

    /**
     * @var EventStorePersister
     */
    private $eventStorePersister;

    /**
     * @param EventStoreRepository $eventStoreRepository
     * @param EventStorePersister $eventStorePersister
     */
    public function __construct(EventStoreRepository $eventStoreRepository, EventStorePersister $eventStorePersister)
    {
        $this->eventStoreRepository = $eventStoreRepository;
        $this->eventStorePersister = $eventStorePersister;
    }

    /**
     * @param AddPaymentToOrderCommand $command
     */
    public function handleAddPaymentToOrderCommand(AddPaymentToOrderCommand $command)
    {
        $order = $this->eventStoreRepository->findOrderById(AggregateId::fromString($command->getOrderId()));

        $order->addPositionPayment(
            UniqIdPositionId::fromString($command->getPositionId()),
            $command->getPayerName(),
            Money::fromString($command->getPaymentAmount(), $command->getPaymentCurrency())
        );

        $this->eventStorePersister->persist($order->getLatestEvents());
    }
}
