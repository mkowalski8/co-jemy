<?php

namespace Infrastructure\CoJemy\Order\Handlers;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\Order;

use Domain\CoJemy\SupplierRepository;
use Infrastructure\CoJemy\Order\Commands\OpenOrderCommand;
use Infrastructure\CoJemy\Order\Exception\SupplierNotFoundException;

class OpenOrderHandler
{
    /**
     * @var EventStorePersister
     */
    private $eventStorePersister;

    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    /**
     * @var Order\OrderHashGenerator
     */
    private $orderHashGenerator;

    /**
     * OpenOrderHandler constructor.
     * @param EventStorePersister $eventStorePersister
     * @param SupplierRepository $supplierRepository
     * @param Order\OrderHashGenerator $orderHashGenerator
     */
    public function __construct(
        EventStorePersister $eventStorePersister,
        SupplierRepository $supplierRepository,
        Order\OrderHashGenerator $orderHashGenerator
    ) {
        $this->eventStorePersister = $eventStorePersister;
        $this->supplierRepository = $supplierRepository;
        $this->orderHashGenerator = $orderHashGenerator;
    }

    /**
     * @param OpenOrderCommand $command
     * @throws SupplierNotFoundException
     */
    public function handleOpenOrderCommand(OpenOrderCommand $command)
    {
        $hashHolder = $this->orderHashGenerator->generate();
        $order = new Order(AggregateId::fromString($command->getAggregateId()));
        $adminId = Order\UserId::fromString($command->getAdminId());

        $supplier = $this->supplierRepository->findSupplier($command->getSupplierId());
        if (!$supplier) {
            throw new SupplierNotFoundException(sprintf(
                'Could not find supplier with id %s.',
                $command->getSupplierId()
            ));
        }
        
        $order->open(
            $command->getSupplierId(),
            $hashHolder,
            $supplier->getPricePerPackage()->getAmount(),
            $supplier->getDeliveryCost()->getAmount(),
            $adminId
        );
        
        $this->eventStorePersister->persist($order->getLatestEvents());
    }
}
