<?php

namespace Infrastructure\CoJemy\Order\Handlers;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order\Dish;
use Domain\CoJemy\Order\Status;
use Domain\CoJemy\Order\UserId;
use Domain\SharedKernel\Date;
use Domain\SharedKernel\Money;
use Domain\SharedKernel\Money\Currency;
use Infrastructure\CoJemy\Order\Commands\AddPositionToOrderCommand;
use Infrastructure\CoJemy\Order\Exception\OrderClosedException;
use Infrastructure\CoJemy\Order\Exception\OrderNotFoundException;

class AddPositionToOrderHandler
{
    /**
     * @var EventStoreRepository
     */
    private $eventStoreRepository;

    /**
     * @var EventStorePersister
     */
    private $eventStorePersister;

    /**
     * @param EventStoreRepository $eventStoreRepository
     * @param EventStorePersister $eventStorePersister
     */
    public function __construct(EventStoreRepository $eventStoreRepository, EventStorePersister $eventStorePersister)
    {
        $this->eventStoreRepository = $eventStoreRepository;
        $this->eventStorePersister = $eventStorePersister;
    }

    /**
     * @param AddPositionToOrderCommand $command
     * @throws OrderClosedException
     * @throws OrderNotFoundException
     */
    public function handleAddPositionToOrderCommand(AddPositionToOrderCommand $command)
    {
        $orderId = $command->getOrderId();
        $order = $this->eventStoreRepository->findOrderById(AggregateId::fromString($orderId));

        if (null === $order) {
            throw new OrderNotFoundException(
                sprintf('There is no order with id %s.', $orderId)
            );
        }

        if ($order->getStatus()->isClosed()) {
            throw new OrderClosedException(
                sprintf('Order with id %s has been closed.', $orderId)
            );
        }

        $userId = $command->getUserId() ? UserId::fromString($command->getUserId()) : UserId::generate();

        $dish = Dish::withId(
            $command->getDishName(),
            $command->getDishId(),
            new Money($command->getPrice(), new Currency($command->getCurrency()))
        );

        $order->addPosition(
            $dish,
            $userId->__toString(),
            $command->getUserNick(),
            new Date()
        );

        $this->eventStorePersister->persist($order->getLatestEvents());
    }
}
