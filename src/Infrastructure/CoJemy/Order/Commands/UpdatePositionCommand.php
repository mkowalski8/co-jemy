<?php

namespace Infrastructure\CoJemy\Order\Commands;

class UpdatePositionCommand
{
    /**
     * @var string
     */
    private $positionId;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $dishId;

    /**
     * @var string
     */
    private $dishName;

    /**
     * @var int
     */
    private $price;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $userNick;

    /**
     * @param string $positionId
     * @param string $orderId
     * @param string $dishId
     * @param string $dishName
     * @param int $price
     * @param string $currency
     * @param string $userNick
     * @param string|null $userId
     */
    public function __construct(
        string $positionId,
        string $orderId,
        string $dishId,
        string $dishName,
        int $price,
        string $currency,
        string $userNick,
        string $userId = null
    ) {
        $this->positionId = $positionId;
        $this->orderId = $orderId;
        $this->userId = $userId;
        $this->dishId = $dishId;
        $this->dishName = $dishName;
        $this->price = $price;
        $this->userNick = $userNick;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getPositionId() : string
    {
        return $this->positionId;
    }

    /**
     * @return string
     */
    public function getOrderId() : string
    {
        return $this->orderId;
    }

    /**
     * @return string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getDishId() : string
    {
        return $this->dishId;
    }

    /**
     * @return string
     */
    public function getDishName() : string
    {
        return $this->dishName;
    }

    /**
     * @return int
     */
    public function getPrice() : int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getUserNick() : string
    {
        return $this->userNick;
    }
}
