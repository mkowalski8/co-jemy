<?php

namespace Infrastructure\CoJemy\Order\Commands;

class OpenOrderCommand
{
    /**
     * @var string
     */
    private $supplierId;

    /**
     * @var string
     */
    private $aggregateId;

    /**
     * @var string
     */
    private $adminId;

    /**
     * @param string $supplierId
     * @param string $aggregateId
     * @param string $adminId
     */
    public function __construct(
        string $supplierId,
        string $aggregateId,
        string $adminId
    ) {
        $this->supplierId = $supplierId;
        $this->aggregateId = $aggregateId;
        $this->adminId = $adminId;
    }

    /**
     * @return string
     */
    public function getSupplierId() : string
    {
        return $this->supplierId;
    }

    public function getAggregateId() : string
    {
        return $this->aggregateId;
    }

    public function getAdminId() : string
    {
        return $this->adminId;
    }
}
