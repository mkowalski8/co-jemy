<?php

namespace Infrastructure\CoJemy\Order\Exception;

class OrderNotFoundException extends \Exception
{
}
