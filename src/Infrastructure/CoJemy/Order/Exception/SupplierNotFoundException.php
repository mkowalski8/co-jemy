<?php

namespace Infrastructure\CoJemy\Order\Exception;

class SupplierNotFoundException extends \Exception
{
}
