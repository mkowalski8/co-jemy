<?php

namespace Infrastructure\CoJemy\Order\Exception;

class OrderClosedException extends \Exception
{
}