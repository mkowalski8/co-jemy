<?php

namespace Bundle\CoJemyCore\CoreBundle\View;

use Application\View;
use Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier;
use Domain\CoJemy\Order\UserId;
use Domain\CoJemy\Order;

class OrderView implements View
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var FoodSupplier
     */
    private $supplier;

    /**
     * @var UserId
     */
    private $userId;

    /**
     * @param Order $order
     * @param FoodSupplier $supplier
     * @param UserId $userId
     */
    public function __construct(Order $order, FoodSupplier $supplier, UserId $userId)
    {
        $this->order = $order;
        $this->supplier = $supplier;
        $this->userId = $userId;
    }

    public function toArray(): array
    {
        $isAdmin = in_array($this->userId, $this->order->getAdminIds());

        $result = [
            'isAdmin' => $isAdmin,
            'id' => (string) $this->order->getAggregateId(),
            'supplier' => $this->supplier
        ];

        if ($isAdmin) {
            $result['adminHash'] = $this->order->getHashHolder()->getAdminHash();
        }

        return $result;
    }
}
