<?php

namespace Bundle\CoJemyCore\CoreBundle\View;

use Application\View;
use Domain\CoJemy\Order\Positions\Position;
use Domain\CoJemy\Order\UserId;

class PositionView implements View
{
    /**
     * @var Position
     */
    private $position;
    /**
     * @var UserId
     */
    private $userId;

    public function __construct(Position $position, UserId $userId)
    {
        $this->position = $position;
        $this->userId = $userId;
    }

    public function toArray(): array
    {
        return [
            'position' => [
                'userId' => (string) $this->userId,
                'id' => (string) $this->position->getId(),
                'dishId' => $this->position->getDish()->getId(),
                'dishName' => $this->position->getDish()->getName(),
                'username' => $this->position->getUserNick(),
                'price' => $this->position->getDish()->getPrice()->getAmount()->getAmount()
            ]
        ];
    }
}
