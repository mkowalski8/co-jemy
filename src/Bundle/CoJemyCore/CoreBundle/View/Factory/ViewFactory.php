<?php

namespace Bundle\CoJemyCore\CoreBundle\View\Factory;

use Bundle\CoJemyCore\CoreBundle\View\OrderView;
use Bundle\CoJemyCore\CoreBundle\View\PositionView;
use Doctrine\ORM\EntityRepository;
use Domain\CoJemy\Order\UserId;
use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStoreRepository;
use Infrastructure\CoJemy\Order\Exception\OrderNotFoundException;
use Infrastructure\CoJemy\Order\Exception\SupplierNotFoundException;

class ViewFactory
{
    /**
     * @var EntityRepository
     */
    private $foodSupplierRepository;

    /**
     * @var EventStoreRepository
     */
    private $eventStoreRepository;

    public function __construct(
        EventStoreRepository $eventStoreRepository,
        EntityRepository $foodSupplierRepository
    ) {
        $this->foodSupplierRepository = $foodSupplierRepository;
        $this->eventStoreRepository = $eventStoreRepository;
    }

    /**
     * @param AggregateId $orderId
     * @param UserId $userId
     * @return OrderView
     * @throws OrderNotFoundException
     * @throws SupplierNotFoundException
     */
    public function createOrderView(AggregateId $orderId, UserId $userId)
    {
        $order = $this->eventStoreRepository->findOrderById($orderId);
        if (!$order) {
            throw new OrderNotFoundException(
                sprintf('Could not find order with id %s', $orderId)
            );
        }

        $supplier = $this->foodSupplierRepository->find($order->getSupplierId());
        if (!$supplier) {
            throw new SupplierNotFoundException(
                sprintf('Could not find supplier with id %s', $order->getSupplierId())
            );
        }

        return new OrderView(
            $order,
            $supplier,
            $userId
        );
    }

    public function createPositionView(AggregateId $orderId, UserId $userId)
    {
        $order = $this->eventStoreRepository->findOrderById($orderId);
        if (!$order) {
            throw new OrderNotFoundException(
                sprintf('Could not find order with id %s', $orderId)
            );
        }

        $positions = $order->getPositions();
        $lastAddedPosition = $positions->getLastAddedPosition();

        return new PositionView(
            $lastAddedPosition,
            $userId
        );
    }
}
