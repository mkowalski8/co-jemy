<?php

namespace Bundle\CoJemyCore\CoreBundle\Repository;

use Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier;
use Doctrine\ORM\EntityRepository;
use Domain\CoJemy\Supplier;
use Domain\CoJemy\SupplierRepository as DomainSupplierRepository;
use Domain\SharedKernel\Money;

class SupplierRepository implements DomainSupplierRepository
{
    private $doctrineSupplierRepository;

    public function __construct(EntityRepository $doctrineSupplierRepository)
    {
        $this->doctrineSupplierRepository = $doctrineSupplierRepository;
    }

    function findSupplier(string $supplierId)
    {
        /** @var FoodSupplier $supplier */
        $supplier = $this->doctrineSupplierRepository->find($supplierId);

        if ($supplier) {
            $currency = new Money\Currency($supplier->getCurrency());

            return new Supplier(
                (string) $supplier->getId(),
                new Money((int) $supplier->getSinglePackageCost(), $currency),
                new Money((int) $supplier->getFreeDeliveryThreshold(), $currency),
                new Money((int) $supplier->getDeliveryCost(), $currency)
            );
        }

        return null;
    }
}
