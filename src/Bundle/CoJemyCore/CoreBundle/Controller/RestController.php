<?php

namespace Bundle\CoJemyCore\CoreBundle\Controller;

use Domain\CoJemy\Exception\Order\PositionNotFoundException;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Infrastructure\CoJemy\Order\Exception\OrderClosedException;
use Infrastructure\CoJemy\Order\Exception\OrderNotFoundException;
use Infrastructure\CoJemy\Order\Exception\SupplierNotFoundException;
use Infrastructure\CoJemy\Order\Commands\AddPositionToOrderCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

use Infrastructure\CoJemy\Order\Commands\OpenOrderCommand;

use Domain\CoJemy\Order\UserId;
use Domain\CoJemy\Aggregate\AggregateId;

class RestController extends FOSRestController
{
    const DEFAULT_CURRENCY = 'PLN';

    public function getSuppliersAction()
    {
        $suppliers = $this->getDoctrine()->getRepository('CoreBundle:FoodSupplier')->findAll();

        if (count($suppliers) === 0) {
            throw new NotFoundHttpException('Suppliers not found.');
        }

        return $suppliers;
    }

    public function getSupplierAction($id)
    {
        $supplier = $this->getDoctrine()->getRepository('CoreBundle:FoodSupplier')->find($id);

        if (is_null($supplier)) {
            throw new NotFoundHttpException('Supplier not found.');
        }

        return $supplier;
    }

    /**
     * @Post("/orders")
     *
     * @param Request $request
     * @param UserId $userId
     *
     * @return View
     */
    public function openOrderAction(Request $request, UserId $userId)
    {
        $orderId = AggregateId::generate();

        try {
            $this->get('tactician.commandbus')->handle(new OpenOrderCommand(
                $request->get('supplierId'),
                (string)$orderId,
                $userId
            ));
        } catch (SupplierNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), $e);
        }

        try {
            $orderView = $this->get('co_jemy.view_factory')->createOrderView($orderId, $userId);
        } catch (OrderNotFoundException $e) {
            throw new NotFoundHttpException(
                $e->getMessage(),
                $e
            );
        } catch (SupplierNotFoundException $e) {
            throw new NotFoundHttpException(
                $e->getMessage(),
                $e
            );
        }

        $view = $this->view(
            array_merge(
                ['userId'=>(string) $userId],
                $orderView->toArray()
            ),
            201
        );

        return $view;
    }

    /**
     * @Post("/orders/{orderId}/positions")
     *
     * @param string $orderId
     * @param Request $request
     * @param UserId $userId
     * @return View
     */
    public function addPositionToOrderAction(string $orderId, Request $request, UserId $userId)
    {
        try {
            $this->get('tactician.commandbus')->handle(new AddPositionToOrderCommand(
                $orderId,
                $request->get('dishId'),
                $request->get('dishName'),
                $request->get('price'),
                self::DEFAULT_CURRENCY,
                $request->get('userNick'),
                $userId
            ));
        } catch(OrderNotFoundException $e) {
            throw new NotFoundHttpException(
                $e->getMessage(),
                $e
            );
        } catch(OrderClosedException $e) {
            throw new UnauthorizedHttpException(
                '',
                $e->getMessage(),
                $e
            );
        }

        $orderId = AggregateId::fromString($orderId);

        try {
            $positionView = $this->get('co_jemy.view_factory')->createPositionView($orderId, $userId);
        } catch(PositionNotFoundException $e) {
            throw new NotFoundHttpException(
                '',
                $e->getMessage(),
                $e
            );
        }

        $view = $this->view($positionView->toArray(), 201);

        return $view;
    }
}
