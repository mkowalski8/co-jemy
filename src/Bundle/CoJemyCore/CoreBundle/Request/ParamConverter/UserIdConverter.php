<?php

namespace Bundle\CoJemyCore\CoreBundle\Request\ParamConverter;

use Domain\CoJemy\Order\UserId;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class UserIdConverter implements ParamConverterInterface
{
    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        if ($request->headers->has('userId') && !empty($request->headers->get('userId'))) {
            $userId = UserId::fromString($request->headers->get('userId'));
        } else {
            $userId = UserId::generate();
        }

        $request->attributes->set(
            $configuration->getName(),
            $userId
        );

        return true;
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === UserId::class;
    }
}
